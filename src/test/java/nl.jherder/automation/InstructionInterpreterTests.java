package nl.jherder.automation;

import nl.jherder.modules.automation.instructions.InstructionInterpreter;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Map;

import static org.junit.Assert.*;

public class InstructionInterpreterTests {

    private static final InstructionInterpreter instructionInterpreter = new InstructionInterpreter();

    @Test
    public void detectFieldsAndPropertiesTest(){
        nl.jherder.automation.TestInstruction instruction = new TestInstruction();
        Field[] fields = instructionInterpreter.getFields(instruction);
        String[] props = instructionInterpreter.getProperties(instruction);
        Map<String, Class> propsMap = instructionInterpreter.getPropertiesMap(instruction);

        assertEquals(3, fields.length);
        assertEquals(3, props.length);
        assertEquals(3, propsMap.size());
    }

    @Test
    public void setAndGetPropertiesTest(){
        TestInstruction instruction = new TestInstruction();

        int currentTestId = instructionInterpreter.getPropertyValue(instruction, "testID");
        String currentTestString = instructionInterpreter.getPropertyValue(instruction, "testString");
        boolean currentTestBoolean = instructionInterpreter.getPropertyValue(instruction, "testBoolean");
        assertEquals(currentTestId, 0);
        assertEquals(currentTestString, null);
        assertEquals(currentTestBoolean, false);

        int newTestID = 10;
        String newTestString = "newTestString";
        boolean newTestBoolean = true;
        instructionInterpreter.setPropertyValue(instruction, "testID", newTestID);
        instructionInterpreter.setPropertyValue(instruction, "testString", newTestString);
        instructionInterpreter.setPropertyValue(instruction, "testBoolean", newTestBoolean);
        assertEquals(instruction.testID, newTestID);
        assertEquals(instruction.testString, newTestString);
        assertEquals(instruction.testBoolean, newTestBoolean);
    }
}
