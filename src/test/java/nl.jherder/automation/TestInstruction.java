package nl.jherder.automation;

import nl.jherder.modules.automation.instructions.Instruction;

public class TestInstruction extends Instruction {

    public int testID;
    public String testString;
    public boolean testBoolean;

}
