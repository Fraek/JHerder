package nl.jherder.core.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class EnumValidator implements ConstraintValidator<ValidateEnum, Enum> {

    List<String> valueList = null;

    @Override
    public boolean isValid(Enum value, ConstraintValidatorContext context) {
        return value != null && valueList.contains(value.toString().toUpperCase());
    }

    @Override
    public void initialize(ValidateEnum constraintAnnotation) {
        valueList = new ArrayList<>();
        Class<? extends Enum<?>> enumClass = constraintAnnotation.enumClass();

        Enum[] enumValArr = enumClass.getEnumConstants();

        for (Enum enumVal : enumValArr) {
            System.out.println(enumVal.toString().toUpperCase());
            valueList.add(enumVal.toString().toUpperCase());
        }
    }
}