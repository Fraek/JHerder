package nl.jherder.core.service;

import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.validation.constraints.NotNull;
import java.util.List;

public abstract class AbstractService<E extends Object> extends AbstractDAO<E>{

    protected Class<? extends E> daoType;

    public AbstractService(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public E findById(Long id) {
        return get(id);
    }

    public E save(@NotNull E entity) {
        return this.persist(entity);
    }

    public E saveOrUpdate(@NotNull E entity) {
        this.currentSession().saveOrUpdate(entity);
        return entity;
    }

    public E update(@NotNull E entity) {
        this.currentSession().update(entity);
        return entity;
    }

    public void delete(E entity) {
        this.currentSession().delete(entity);
    }

    public List<E> findAll() {
        return null;
//        return this.currentSession().createQuery("", E).getResultList();
    }

    protected CriteriaQuery<? extends E> createCriteriaQuery(){
        return this.getCriteriaBuilder().createQuery(daoType);
    }

    protected CriteriaBuilder getCriteriaBuilder(){
        return this.currentSession().getCriteriaBuilder();
    }

}
