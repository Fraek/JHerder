package nl.jherder.core.converters;

import nl.jherder.modules.domotica.defines.Type;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;

public class IntToTypeConverter implements Converter<Integer, Type>{

    @Override
    public Type convert(MappingContext<Integer, Type> context) {
        switch (context.getSource()) {
            case 1:
                return Type.SENSOR;
            case 2:
                return Type.ACTOR;
            case 3:
                return Type.IGNORED;
            default:
                return Type.UNKNOWN;
        }
    }

}
