package nl.jherder.core.converters;

import nl.jherder.modules.domotica.defines.Status;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;

public class IntToStatusConverter implements Converter<Integer, Status> {

    @Override
    public Status convert(MappingContext<Integer, Status> context) {
        switch (context.getSource()) {
            case 1:
                return Status.ON;
            case 2:
                return Status.OFF;
            default:
                return Status.UNKNOWN;
        }
    }
}