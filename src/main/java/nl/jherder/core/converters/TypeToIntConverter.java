package nl.jherder.core.converters;

import nl.jherder.modules.domotica.defines.Type;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;

public class TypeToIntConverter implements Converter<Type, Integer>{

    @Override
    public Integer convert(MappingContext<Type, Integer> context) {
        switch (context.getSource()) {
            case SENSOR:
                return 1;
            case ACTOR:
                return 2;
            case IGNORED:
                return 3;
            default:
                return 0;
        }
    }

}
