package nl.jherder.core.converters;

import nl.jherder.modules.domotica.defines.Status;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;

public class StatusToIntConverter implements Converter<Status, Integer> {

    @Override
    public Integer convert(MappingContext<Status, Integer> context) {
        switch (context.getSource()) {
            case ON:
                return 1;
            case OFF:
                return 2;
            default:
                return 0;
        }
    }
}