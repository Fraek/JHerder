package nl.jherder.core.bundles;

import com.google.inject.AbstractModule;
import com.google.maps.GeoApiContext;
import nl.jherder.AppConfiguration;
import ru.vyarus.dropwizard.guice.module.support.ConfigurationAwareModule;

public class GeoApiBundle extends AbstractModule implements ConfigurationAwareModule<AppConfiguration>{

    private String googleApiKey;

    @Override
    protected void configure() {

        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey(googleApiKey)
                .build();

        bind(GeoApiContext.class).toInstance(context);
    }

    @Override
    public void setConfiguration(AppConfiguration appConfiguration) {
        this.googleApiKey = appConfiguration.getGoogleApiKey();
    }
}
