package nl.jherder.core.bundles;

import com.google.inject.Inject;
import de.spinscale.dropwizard.jobs.GuiceJobManager;
import de.spinscale.dropwizard.jobs.Job;
import de.spinscale.dropwizard.jobs.JobConfiguration;
import de.spinscale.dropwizard.jobs.JobsBundle;
import io.dropwizard.setup.Environment;
import ru.vyarus.dropwizard.guice.GuiceBundle;

public class GuiceJobsBundle extends JobsBundle {

    private final GuiceBundle guiceBundle;

    @Inject
    public GuiceJobsBundle(final GuiceBundle guiceBundle) {
        super(new Job[0]);
        this.guiceBundle = guiceBundle;
    }

    @Override
    public void run(JobConfiguration configuration, Environment environment) throws Exception {
        GuiceJobManager guiceJobsManager = new GuiceJobManager(guiceBundle.getInjector());
        guiceJobsManager.configure(configuration);
        environment.lifecycle().manage(guiceJobsManager);
    }
}
