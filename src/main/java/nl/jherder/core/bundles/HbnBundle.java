package nl.jherder.core.bundles;

import com.google.common.collect.ImmutableList;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.SessionFactoryFactory;
import nl.jherder.AppConfiguration;
import nl.jherder.jdbi.IEntity;
import org.reflections.Reflections;

import java.util.Set;

public class HbnBundle extends HibernateBundle<AppConfiguration> {

    public HbnBundle() {
        super(HbnBundle.getJDBIPackages(), new SessionFactoryFactory());
    }

    @Override
    public PooledDataSourceFactory getDataSourceFactory(AppConfiguration configuration) {
        return configuration.getDataSourceFactory();
    }

    private static ImmutableList<Class<?>> getJDBIPackages() {
        Reflections reflections = new Reflections("nl.jherder.jdbi");
        Set<Class<? extends IEntity>> entities = reflections.getSubTypesOf(IEntity.class);

        ImmutableList.Builder<Class<?>> builder = ImmutableList.builder();
        builder.addAll(entities);

        return builder.build();
    }

}