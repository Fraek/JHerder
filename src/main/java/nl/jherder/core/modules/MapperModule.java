package nl.jherder.core.modules;

import com.google.inject.AbstractModule;
import nl.jherder.core.mapping.IMap;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.reflections.Reflections;
import java.util.Set;

public class MapperModule extends AbstractModule {

    private static ModelMapper modelMapper;

    @Override
    protected void configure() {
        initModelMapper();
        bind(ModelMapper.class).toInstance(modelMapper);
        applyMappings();
    }

    private void initModelMapper() {
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setFieldMatchingEnabled(true);
    }

    private void applyMappings() {
        Reflections reflections = new Reflections("nl.jherder");
        Set<Class<? extends IMap>> maps = reflections.getSubTypesOf(IMap.class);

        maps.forEach(m -> {
            try {
                IMap<?,?> instance = (IMap<?,?>) m.newInstance();
                PropertyMap map = instance.getMapping();

                if(map != null)
                    modelMapper.addMappings(map);

            } catch (IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
        });
    }

}
