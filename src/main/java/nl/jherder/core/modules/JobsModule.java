package nl.jherder.core.modules;

import com.google.inject.AbstractModule;
import nl.jherder.core.bundles.HbnBundle;
import nl.jherder.jobs.ExampleJob;
import org.hibernate.SessionFactory;

public class JobsModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ExampleJob.class);
    }
}
