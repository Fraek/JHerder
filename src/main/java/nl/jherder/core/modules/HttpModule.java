package nl.jherder.core.modules;

import io.dropwizard.client.HttpClientBuilder;
import nl.jherder.AppConfiguration;
import org.apache.http.client.HttpClient;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

public class HttpModule extends DropwizardAwareModule<AppConfiguration> {

    @Override
    protected void configure() {
        HttpClient httpClient = new HttpClientBuilder(environment()).
                using(configuration().getHttpClientConfiguration()).build("http-client");

        bind(HttpClient.class).toInstance(httpClient);
    }
}
