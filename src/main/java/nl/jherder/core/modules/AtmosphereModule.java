package nl.jherder.core.modules;

import nl.jherder.AppConfiguration;
import nl.jherder.modules.conversation.ConversationHandler;
import org.atmosphere.cpr.ApplicationConfig;
import org.atmosphere.cpr.AtmosphereServlet;
import org.atmosphere.cpr.BroadcasterFactory;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

import javax.servlet.ServletRegistration;

//@See: https://www.mkammerer.de/blog/dropwizard-and-atmosphere/
public class AtmosphereModule extends DropwizardAwareModule<AppConfiguration> {

    @Override
    protected void configure() {
        AtmosphereServlet servlet = new AtmosphereServlet();
        servlet.framework().addInitParameter(ApplicationConfig.ANNOTATION_PACKAGE, ConversationHandler.class.getPackage().getName());
        servlet.framework().addInitParameter(ApplicationConfig.WEBSOCKET_SUPPORT, "true");
        bind(AtmosphereServlet.class).toInstance(servlet);

        BroadcasterFactory broadcasterFactory = servlet.framework().getBroadcasterFactory();
        bind(BroadcasterFactory.class).toInstance(broadcasterFactory);

        ServletRegistration.Dynamic registration = environment().servlets().addServlet("atmosphere", servlet);
        registration.addMapping("/ws/*");
    }
}
