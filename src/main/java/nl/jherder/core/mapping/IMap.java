package nl.jherder.core.mapping;

import org.modelmapper.PropertyMap;

public interface IMap<Source, Destination> {

    PropertyMap<Source, Destination> getMapping();

}