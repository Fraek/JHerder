package nl.jherder.core.util;

import org.joda.time.DateTime;
import org.joda.time.Days;

public class TimeUtil {

    public static boolean isToday(DateTime dateTime) {
        return dateTime.withTimeAtStartOfDay().equals(DateTime.now().withTimeAtStartOfDay());
    }

    public static boolean isAfterToday(DateTime dateTime) {
        return dateTime.isAfter(DateTime.now().withTimeAtStartOfDay().plusHours(24));
    }

    public static boolean isTommorow(DateTime dateTime) {
        return dateTime.withTimeAtStartOfDay().equals(DateTime.now().withTimeAtStartOfDay().plusDays(1));
    }

    public static boolean isYesterday(DateTime dateTime) {
        return dateTime.withTimeAtStartOfDay().equals(DateTime.now().withTimeAtStartOfDay().minusDays(1));
    }

    public static boolean isWithinHours(DateTime dateTime, int hours) {
        boolean afterNow = dateTime.plusMinutes(1).isAfterNow();
        return  afterNow && dateTime.isBefore(DateTime.now().getMillis() + hoursToMillis(hours));
    }

    public static int getDaysFromNow(DateTime end){
        return Days.daysBetween(DateTime.now().withTimeAtStartOfDay(),end.withTimeAtStartOfDay()).getDays();
    }

    public static long hoursToMillis(int hours) {
        return hours * 3600000;
    }
}
