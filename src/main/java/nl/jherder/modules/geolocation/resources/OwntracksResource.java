package nl.jherder.modules.geolocation.resources;

import io.dropwizard.hibernate.UnitOfWork;
import nl.jherder.jdbi.UserLocationEntity;
import nl.jherder.jdbi.UserEntity;
import nl.jherder.modules.geolocation.models.OwntracksLocation;
import nl.jherder.modules.geolocation.services.UserLocationService;
import nl.jherder.modules.user.services.UserService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.Timestamp;

@Path("/owntracks/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OwntracksResource {

    @Inject
    private UserLocationService locationService;

    @Inject
    private UserService userService;

    @POST
    @UnitOfWork
    public UserLocationEntity saveLocation(OwntracksLocation owntracksLocation) {

        UserEntity user = userService.findById((long)owntracksLocation.getTid());

        UserLocationEntity userLocationEntity = new UserLocationEntity();
        userLocationEntity.setCreationTime(new Timestamp((long)owntracksLocation.getTst() * 1000));
        userLocationEntity.setUserByUserId(user);
        userLocationEntity.setLat(owntracksLocation.getLat());
        userLocationEntity.setLon(owntracksLocation.getLon());
        userLocationEntity.setBattery(owntracksLocation.getBatt());
        userLocationEntity.setAccuracy(owntracksLocation.getAcc());
        userLocationEntity.setConnection(owntracksLocation.getConn());

        return locationService.save(userLocationEntity);
    }
}
