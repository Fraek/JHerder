package nl.jherder.modules.geolocation.resources;

import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import io.dropwizard.hibernate.UnitOfWork;
import nl.jherder.jdbi.UserEntity;
import nl.jherder.jdbi.UserLocationEntity;
import nl.jherder.modules.geolocation.services.UserLocationService;
import nl.jherder.modules.user.services.UserService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Path("/user-location/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserLocationResource {

    @Inject
    private UserService userService;

    @Inject
    private UserLocationService locationService;

    @GET
    @UnitOfWork
    @Path("/current/{userId}")
    public GeocodingResult getUserAddress(@PathParam("userId") long userId) throws IOException, ApiException, InterruptedException {

        UserEntity user = userService.findById(userId);

        if(user == null){
            throw new NotFoundException("Couldn't find user");
        }

        UserLocationEntity lastLocation = locationService.getLastUserLocation(user);

        if(lastLocation == null){
            throw new NotFoundException("Last location of " + user.getFirstName() + " is unknown");
        }

        return locationService.getAddress(lastLocation);
    }

}
