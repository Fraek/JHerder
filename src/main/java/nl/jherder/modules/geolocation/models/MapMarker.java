package nl.jherder.modules.geolocation.models;

import com.google.maps.model.LatLng;

public class MapMarker {

    private LatLng latLng;
    private char label;
    private String color = "red";

    public MapMarker(LatLng latLng, char label) {
        this.latLng = latLng;
        this.label = label;
    }

    public MapMarker(LatLng latLng, char label, String color) {
        this.latLng = latLng;
        this.label = label;
        this.color = color;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public char getLabel() {
        return Character.toUpperCase(label);
    }

    public void setLabel(char label) {
        this.label = label;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
