package nl.jherder.modules.geolocation.models;

import com.google.inject.Injector;
import com.google.maps.model.LatLng;
import nl.jherder.AppConfiguration;
import nl.jherder.Application;

import java.util.ArrayList;
import java.util.List;

public class StaticMapRequest {

    public static final String MAPTYPE_ROADMAP = "roadmap";
    public static final String MAPTYPE_TERAIN = "terrain";
    public static final String MAPTYPE_SATELLITE = "satellite";
    public static final String MAPTYPE_HYBRID = "hybrid";

    private static final String BASE_URL = "https://maps.googleapis.com/maps/api/staticmap";

    private LatLng latLng;
    private int zoomLevel;
    private int width = 600;
    private int height = 600;
    private List<MapMarker> markers = new ArrayList<>();
    private String mapType = MAPTYPE_ROADMAP;

    public StaticMapRequest(LatLng latLng, int zoomLevel) {
        this.latLng = latLng;
        this.zoomLevel = zoomLevel;
    }

    public String getImageUrl() {

        return BASE_URL +
                "?center=" + latLng.lat + "," + latLng.lng +
                "&zoom=" + zoomLevel +
                "&size=" + width + "x" + height +
                "&maptype=" + mapType +
                getMarkersQuery() +
                "&key=" + getApiKey();
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public int getZoomLevel() {
        return zoomLevel;
    }

    public void setZoomLevel(int zoomLevel) {
        this.zoomLevel = zoomLevel;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public List<MapMarker> getMarkers() {
        return markers;
    }

    public void addMarker(MapMarker marker) {
        this.markers.add(marker);
    }

    public String getMapType() {
        return mapType;
    }

    public void setMapType(String mapType) {
        this.mapType = mapType;
    }

    private String getMarkersQuery() {

        String query = "";

        for (MapMarker marker : markers) {
            LatLng latLng = marker.getLatLng();
            query += "&markers=label:" + marker.getLabel() + "|color:" + marker.getColor() + "|" + latLng.lat + "," + latLng.lng;
        }

        return query;
    }

    private String getApiKey() {
        Injector injector = Application.getInjector();
        AppConfiguration appConfiguration = injector.getInstance(AppConfiguration.class);
        return appConfiguration.getGoogleApiKey();
    }

}
