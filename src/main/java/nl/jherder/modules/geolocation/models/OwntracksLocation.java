package nl.jherder.modules.geolocation.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OwntracksLocation {

    private int tst;
    private int acc;
    private int batt;
    private float lat;
    private float lon;
    private String tid;
    private String conn;

    public int getTid() {
        return Integer.parseInt(tid);
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public int getTst() {
        return tst;
    }

    public void setTst(int tst) {
        this.tst = tst;
    }

    public int getAcc() {
        return acc;
    }

    public void setAcc(int acc) {
        this.acc = acc;
    }

    public int getBatt() {
        return batt;
    }

    public void setBatt(int batt) {
        this.batt = batt;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public String getConn() {
        return conn;
    }

    public void setConn(String con) {
        this.conn = con;
    }
}
