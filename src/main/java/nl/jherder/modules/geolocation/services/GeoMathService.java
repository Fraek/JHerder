package nl.jherder.modules.geolocation.services;

import com.google.inject.Inject;
import com.spatial4j.core.distance.DistanceUtils;
import com.spatial4j.core.distance.GeodesicSphereDistCalc;
import com.spatial4j.core.shape.Point;

public class GeoMathService {

    private final int KM_IN_METERS = 1000;

    @Inject
    private GeodesicSphereDistCalc.Haversine distanceCalculator;

    public double distanceInKm(Point from, Point to) {
        double radiusDEG = distanceCalculator.distance(from, to);
        return (radiusDEG * DistanceUtils.DEG_TO_KM);
    }

    public double distanceInMeters(Point from, Point to) {
        return distanceInKm(from, to) * KM_IN_METERS;
    }

    public boolean withinPointRadius(Point lookup, Point reference, double radiusInMeters ){
        double radiusDEG = (radiusInMeters / KM_IN_METERS) * DistanceUtils.KM_TO_DEG;
        return distanceCalculator.within(lookup, reference.getX(), reference.getY(), radiusDEG);
    }


}
