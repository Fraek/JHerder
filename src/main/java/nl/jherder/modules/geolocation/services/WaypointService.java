package nl.jherder.modules.geolocation.services;

import nl.jherder.core.service.AbstractService;
import nl.jherder.jdbi.WaypointEntity;
import org.hibernate.SessionFactory;

import javax.inject.Inject;

public class WaypointService extends AbstractService<WaypointEntity> {

    @Inject
    public WaypointService(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
