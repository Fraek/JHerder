package nl.jherder.modules.geolocation.services;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.spatial4j.core.context.SpatialContext;
import com.spatial4j.core.shape.Point;
import com.spatial4j.core.shape.impl.PointImpl;
import nl.jherder.core.service.AbstractService;
import nl.jherder.jdbi.UserEntity;
import nl.jherder.jdbi.UserLocationEntity;
import nl.jherder.jdbi.WaypointEntity;
import nl.jherder.modules.geolocation.models.MapMarker;
import nl.jherder.modules.geolocation.models.StaticMapRequest;
import nl.jherder.modules.user.services.UserService;
import nl.jherder.modules.user.services.UserWaypointService;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.joda.time.Seconds;

import javax.inject.Inject;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class UserLocationService extends AbstractService<UserLocationEntity> {

    private static final int minimalSecondsBetweenReports = 60;
    private static final int maximumSecondsBetweenReports = 60 * 60 * 2;
    private static final int minimalMetersBetweenReports = 50;
    private static final int minimalAccuracy = 105;

    private static HashMap<Long, UserLocationEntity> lastEntities = new HashMap<>();

    @Inject
    public UserLocationService(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Inject
    private GeoMathService geoMathService;

    @Inject
    private GeoApiContext geoApiContext;

    @Inject
    private UserWaypointService userWaypointService;

    @Override
    public UserLocationEntity save(UserLocationEntity entity) {

        if (canSave(entity)) {
            UserLocationEntity newUserLocationEntity = super.save(entity);
            lastEntities.put(newUserLocationEntity.getUserByUserId().getId(), newUserLocationEntity);
            return newUserLocationEntity;
        }

        return UserLocationEntity.Default();
    }

    public boolean isAtWaypoint(UserLocationEntity userLocationEntity, WaypointEntity waypointEntity) {

        Point locationPoint = new PointImpl(userLocationEntity.getLat(), userLocationEntity.getLon(), SpatialContext.GEO);
        Point waypoint = new PointImpl(waypointEntity.getLat(), waypointEntity.getLon(), SpatialContext.GEO);
        return geoMathService.withinPointRadius(locationPoint, waypoint, waypointEntity.getRadius());
    }

    public UserLocationEntity getLastUserLocation(UserEntity user) {
        UserLocationEntity lastEntityFromMemory = lastEntities.get(user.getId());
        if (lastEntityFromMemory != null) {
            return lastEntityFromMemory;
        }

        return findLatestByUser(user);
    }

    public GeocodingResult getAddress(UserLocationEntity userLocationEntity) throws IOException, InterruptedException, ApiException {

        LatLng latLng = new LatLng(userLocationEntity.getLat(), userLocationEntity.getLon());
        GeocodingResult[] results = GeocodingApi.reverseGeocode(geoApiContext, latLng).await();

        return results[0];
    }

    public UserLocationEntity findLatestByUser(UserEntity user) {
        List<UserLocationEntity> latestEntities = findAllByUser(user);
        if (latestEntities.size() > 0) {
            return latestEntities.get(0);
        }

        return null;
    }

    public List<UserLocationEntity> findAllByUser(UserEntity user) {
        return list(namedQuery("UserLocationEntity.findAllByUser")
                .setParameter("user", user));
    }

    public boolean isUserAtHome(UserEntity user) {
        UserLocationEntity lastLocation = getLastUserLocation(user);

        if (lastLocation != null) {
            WaypointEntity userHome = userWaypointService.getUserHomeWaypoint(user);
            return isAtWaypoint(lastLocation, userHome);
        }

        return false;
    }

    public boolean isUserAtWork(UserEntity user) {
        UserLocationEntity lastLocation = getLastUserLocation(user);

        if (lastLocation != null) {
            WaypointEntity userHome = userWaypointService.getUserWorkWaypoint(user);
            return isAtWaypoint(lastLocation, userHome);
        }

        return false;
    }

    private boolean canSave(UserLocationEntity userLocationEntity) {

        return isValidReport(userLocationEntity);
    }

    private boolean isValidReport(UserLocationEntity userLocationEntity) {

        UserEntity userEntity = userLocationEntity.getUserByUserId();
        if (userEntity == null) {
            return false;
        }

        if (!hasValidAccuracy(userLocationEntity)) {
            return false;
        }

        UserLocationEntity lastEntityForUser = lastEntities.get(userEntity.getId());
        if (lastEntityForUser == null) {
            return true;
        }

        int secondsSinceLastReport = getTimeDifference(lastEntityForUser, userLocationEntity);

        if (isTimeToReportAnyWay(secondsSinceLastReport)) {
            return true;
        }

        boolean hasValidTimeChange = hasValidTimeChange(secondsSinceLastReport);
        boolean hasValidDistanceChange = hasValidDistanceChange(lastEntityForUser, userLocationEntity);

        return hasValidTimeChange && hasValidDistanceChange;
    }

    private boolean isTimeToReportAnyWay(int secondsSinceLastReport) {
        return secondsSinceLastReport > maximumSecondsBetweenReports;
    }

    private boolean hasValidTimeChange(int seconds) {
        return seconds > minimalSecondsBetweenReports;
    }

    private boolean hasValidDistanceChange(UserLocationEntity lastEntityForUser, UserLocationEntity userLocationEntity) {

        Point locationPoint = new PointImpl(userLocationEntity.getLat(), userLocationEntity.getLon(), SpatialContext.GEO);
        Point lastLocationPoint = new PointImpl(lastEntityForUser.getLat(), lastEntityForUser.getLon(), SpatialContext.GEO);

        double distanceInMeters = geoMathService.distanceInMeters(locationPoint, lastLocationPoint);
        return distanceInMeters >= minimalMetersBetweenReports;
    }

    private boolean hasValidAccuracy(UserLocationEntity userLocationEntity) {

        return userLocationEntity.getAccuracy() <= minimalAccuracy;
    }

    private int getTimeDifference(UserLocationEntity lastEntityForUser, UserLocationEntity userLocationEntity) {

        DateTime lastEntityTime = new DateTime(lastEntityForUser.getCreationTime());
        DateTime entityTime = new DateTime(userLocationEntity.getCreationTime());

        return Seconds.secondsBetween(lastEntityTime, entityTime).getSeconds();
    }


    public StaticMapRequest getStaticMapRequest(UserLocationEntity lastLocation) {

        LatLng latLng = new LatLng(lastLocation.getLat(), lastLocation.getLon());
        char firstLetter = lastLocation.getUserByUserId().getFirstName().charAt(0);
        MapMarker marker = new MapMarker(latLng, firstLetter);
        StaticMapRequest request = new StaticMapRequest(latLng, 15);
        request.addMarker(marker);

        return request;
    }

    public String getGoogleMapsUrl(UserLocationEntity lastLocation) {
        return "https://www.google.nl/maps/place/" + lastLocation.getLat() + "," + lastLocation.getLon();
    }
}
