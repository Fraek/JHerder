package nl.jherder.modules.automation.instructions;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class InstructionInterpreter {

    public Field[] getFields(Instruction instruction) {
        return instruction.getClass().getFields();
    }

    public boolean setPropertyValue(Instruction instruction, String key, Object value){
        try {
            Field field = instruction.getClass().getDeclaredField(key);
            field.setAccessible(true);
            field.set(instruction, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            return false;
        }
        return true;
    }

    public <V> V getPropertyValue(Instruction instruction, String key) {
        try {
            Field field = instruction.getClass().getDeclaredField(key);
            field.setAccessible(true);
            return (V) field.get(instruction);
        } catch (NoSuchFieldException | IllegalAccessException  e) {
            return null;
        }
    }

    public String[] getProperties(Instruction instruction) {
        Field[] fields = getFields(instruction);
        String[] properties = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            properties[i] = fields[i].getName();
        }
        return properties;
    }

    public Map<String, Class> getPropertiesMap(Instruction instruction){
        Field[] fields = getFields(instruction);
        Map<String, Class> propertiesMap = new HashMap<>(fields.length);
        for (int i = 0; i < fields.length; i++) {
            propertiesMap.put(fields[i].getName(), fields[i].getType());
        }
        return propertiesMap;
    }
}
