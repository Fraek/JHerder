package nl.jherder.modules.automation.instructions;

import java.util.Dictionary;

public interface IInstruction {

    InstructionType getType();

}
