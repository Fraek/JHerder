package nl.jherder.modules.automation.instructions;

public abstract class Instruction implements IInstruction {

    protected InstructionType type = InstructionType.DOMOTICZ;

    @Override
    public InstructionType getType() {
        return type;
    }

}
