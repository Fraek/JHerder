package nl.jherder.modules.automation.instructions;

import nl.jherder.jdbi.DeviceEntity;

public class OffInstruction extends Instruction {

    public DeviceEntity device;

    public OffInstruction(DeviceEntity deviceModel) {
        this.device = deviceModel;
    }
}