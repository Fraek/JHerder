package nl.jherder.modules.automation.instructions;

import nl.jherder.jdbi.DeviceEntity;

public class OnInstruction extends Instruction {

    public DeviceEntity device;

    public OnInstruction(DeviceEntity deviceModel) {
        this.device = deviceModel;
    }
}