package nl.jherder.modules.automation.services;

import nl.jherder.modules.automation.commands.CommandFactoryBuilder;
import nl.jherder.modules.automation.commands.interfaces.ICommand;
import nl.jherder.modules.automation.commands.interfaces.ICommandFactory;
import nl.jherder.modules.automation.instructions.IInstruction;

import javax.inject.Inject;

public class CommandService {

    @Inject
    private CommandFactoryBuilder commandFactoryBuilder;

    public void execute(IInstruction instruction){
        try {
            ICommandFactory commandFactory = commandFactoryBuilder.build(instruction);
            ICommand command = commandFactory.create(instruction);
            command.execute();
        }catch (Exception e){
            System.out.print(e.getMessage());
        }
    }

}
