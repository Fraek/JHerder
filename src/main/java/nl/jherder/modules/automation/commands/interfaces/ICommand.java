package nl.jherder.modules.automation.commands.interfaces;

public interface ICommand {

    void execute();
}
