package nl.jherder.modules.automation.commands;

import com.google.inject.Inject;
import nl.jherder.modules.automation.commands.domoticz.DomoticzCommandFactory;
import nl.jherder.modules.automation.commands.domoticz.DomoticzService;
import nl.jherder.modules.automation.commands.interfaces.ICommandFactory;
import nl.jherder.modules.automation.instructions.IInstruction;

public class CommandFactoryBuilder {

    @Inject
    public DomoticzService domoticzService;

    public ICommandFactory build(IInstruction instruction) {
        switch (instruction.getType()) {
            case DOMOTICZ:
                return new DomoticzCommandFactory(domoticzService);
            default:
                return new DefaultCommandFactory();
        }
    }
}
