package nl.jherder.modules.automation.commands.domoticz;

import nl.jherder.modules.automation.commands.interfaces.IOnCommand;


public class DomoticzOnCommand implements IOnCommand {

    private String idx;

    private DomoticzService domoticzService;

    public DomoticzOnCommand(String idx, DomoticzService domoticzService) {
        this.idx = idx;
        this.domoticzService = domoticzService;
    }

    @Override
    public void execute() {
        domoticzService.switchCommand(this.idx, "On");
    }
}