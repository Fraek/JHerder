package nl.jherder.modules.automation.commands.domoticz;

import nl.jherder.modules.automation.commands.interfaces.IOnCommand;

import javax.inject.Inject;

public class DomoticzOffCommand implements IOnCommand {

    private String idx;

    private DomoticzService domoticzService;

    public DomoticzOffCommand(String idx, DomoticzService domoticzService) {
        this.idx = idx;
        this.domoticzService = domoticzService;
    }

    @Override
    public void execute() {
        domoticzService.switchCommand(this.idx, "Off");
    }
}
