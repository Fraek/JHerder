package nl.jherder.modules.automation.commands.domoticz;

import nl.jherder.modules.automation.commands.DefaultCommandFactory;
import nl.jherder.modules.automation.instructions.IInstruction;
import nl.jherder.modules.automation.commands.interfaces.ICommand;
import nl.jherder.modules.automation.instructions.OffInstruction;
import nl.jherder.modules.automation.instructions.OnInstruction;

public class DomoticzCommandFactory extends DefaultCommandFactory {

    protected DomoticzService domoticzService;

    public DomoticzCommandFactory(DomoticzService domoticzService) {
        this.domoticzService = domoticzService;
    }

    @Override
    public ICommand create(IInstruction instruction) throws Exception {

        if(instruction instanceof OnInstruction)
            return createOnCommand((OnInstruction) instruction);
        if(instruction instanceof OffInstruction)
            return createOffCommand((OffInstruction) instruction);

        throw new Exception("Unknown Domoticz instruction");
    }

    private DomoticzOnCommand createOnCommand(OnInstruction instruction){
        return new DomoticzOnCommand(instruction.device.getReference(), domoticzService);
    }

    private DomoticzOffCommand createOffCommand(OffInstruction instruction){
        return new DomoticzOffCommand(instruction.device.getReference(), domoticzService);
    }

}
