package nl.jherder.modules.automation.commands.interfaces;

import nl.jherder.modules.automation.instructions.IInstruction;

public interface ICommandFactory {

    ICommand create(IInstruction instruction) throws Exception;

}