package nl.jherder.modules.automation.commands;

import nl.jherder.modules.automation.commands.interfaces.ICommandFactory;
import nl.jherder.modules.automation.commands.interfaces.ICommand;
import nl.jherder.modules.automation.instructions.IInstruction;

public class DefaultCommandFactory implements ICommandFactory {

    @Override
    public ICommand create(IInstruction instruction) throws Exception {
        return null;
    }
}
