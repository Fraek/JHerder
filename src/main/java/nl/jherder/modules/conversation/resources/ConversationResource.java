package nl.jherder.modules.conversation.resources;

import com.eclipsesource.json.JsonObject;
import com.google.gson.Gson;
import io.dropwizard.hibernate.UnitOfWork;
import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.conversation.models.ConversationRequest;
import nl.jherder.modules.conversation.models.ConversationResponse;
import nl.jherder.modules.conversation.models.WitResponse;
import nl.jherder.modules.conversation.services.Interpreter;
import nl.jherder.modules.conversation.services.WitService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/conversation")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ConversationResource {

    @Inject
    private WitService Wit;
    @Inject
    private Interpreter interpreter;
    @Inject
    private ContextService contextService;

    @POST
    @UnitOfWork
    @Path("/say")
    public Response say(@NotNull ConversationRequest requestModel) {
        try {
            String message = requestModel.input;
            contextService.setUserInput(message);
            WitResponse witResponse = Wit.sendMessage(message);
            ConversationResponse response = interpreter.run(witResponse);

            Gson gson = new Gson();
            String jsonString =  gson.toJson(response);

            return Response.ok(jsonString).build();
        } catch (Exception exception) {
            ConversationResponse response = new ConversationResponse("Er is iets misgegaan");
            Gson gson = new Gson();
            String jsonString =  gson.toJson(response);
            return Response.ok(jsonString).build();
        }
    }

}