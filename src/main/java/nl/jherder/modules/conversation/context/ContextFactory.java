package nl.jherder.modules.conversation.context;

import nl.jherder.modules.conversation.models.WitEntity;

public class ContextFactory {

    public static Context create(String name, WitEntity entity) {
        switch (name) {
            case "datetime":
                return new DateTimeContext(entity.value);
            default:
                return new DefaultContext(name, entity.value);
        }
    }

}
