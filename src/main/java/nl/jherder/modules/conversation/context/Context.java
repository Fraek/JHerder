package nl.jherder.modules.conversation.context;

import org.joda.time.DateTime;
import org.joda.time.Seconds;

public abstract class Context {

    public enum Type {
        Intent,
        Zone,
        PowerSwitch,
        Location,
        DateTime,
        Tense,
        UserCurrentLocation
    }

    public String name;
    protected String value;
    protected Seconds expirationTime = Seconds.seconds(300);
    protected int lifespan;
    private DateTime created;
    private boolean active = true;

    public Context(String name, String value) {
        this.name = name;
        this.value = value;
        this.created = DateTime.now();
        this.lifespan = 1;
    }

    public Context(String name, String value, int lifespan) {
        this.name = name;
        this.value = value;
        this.created = DateTime.now();
        this.lifespan = lifespan;
    }

    public Seconds getTimeAlive() {
        return Seconds.secondsBetween(DateTime.now(), this.created);
    }

    public void setLifespan(int lifespan) {
        this.lifespan = lifespan;
        this.active = this.lifespan > 0;
    }

    void spend() {
        if (this.lifespan > 0) {
            this.lifespan--;
        }

        if (this.lifespan == 0) {

        }
    }

    public String getValue() {
        return this.value;
    }

    /**
     * Wordt aangeroepen als alle contexten zijn ingeladen, zodat context afhankelijke contexten hun waardes kunnen updaten
     */
    public abstract void evaluate();

    public void setActive(boolean active) {
        this.active = active;
    }


    public boolean isActive() {
        return active;
    }

}
