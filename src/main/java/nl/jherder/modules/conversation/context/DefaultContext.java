package nl.jherder.modules.conversation.context;

public class DefaultContext extends Context {

    public DefaultContext(String name, String value) {
        super(name, value);
    }

    public DefaultContext(String name, String value, int lifespan) {
        super(name, value, lifespan);
    }

    @Override
    public void evaluate() {

    }
}
