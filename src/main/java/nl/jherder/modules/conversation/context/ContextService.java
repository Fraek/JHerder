package nl.jherder.modules.conversation.context;

import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Singleton
public class ContextService {

    private HashMap<String, Context> contexts;
    private String userInput;

    public ContextService() {
        this.contexts = new LinkedHashMap<>();
    }

    public void setContext(Context context) {
        contexts.put(context.name, context);
    }

    public Context getContext(String name) {
        return this.contexts.get(name);
    }

    public void spend() {

        ArrayList<String> toDelete = new ArrayList<>();

        for (Map.Entry<String, Context> entry : contexts.entrySet()) {
            entry.getValue().spend();
            if (entry.getValue().lifespan == 0) {
                toDelete.add(entry.getKey());
            }
        }

        toDelete.forEach(key -> {
            contexts.remove(key);
        });

    }

    public void evaluate() {
        for (Map.Entry<String, Context> entry : contexts.entrySet()) {
            entry.getValue().evaluate();
        }
    }

    public void reset(String name) {
        this.contexts.remove(name);
    }

    public void reset() {
        this.contexts.clear();
    }

    public void setUserInput(String input) {
        this.userInput = input;
    }

    public String getUserInput() {
        return userInput;
    }
}
