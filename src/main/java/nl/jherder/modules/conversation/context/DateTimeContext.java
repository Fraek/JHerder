package nl.jherder.modules.conversation.context;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import javax.inject.Inject;

public class DateTimeContext extends Context {

    @Inject
    public ContextService contextService;
    protected DateTime dateTimeValue;

    public DateTimeContext(String value) {
        super("datetime", value);
        this.dateTimeValue = userInputToDateTime();
    }

    @Override
    public void evaluate() {
        this.dateTimeValue = userInputToDateTime();
    }

    public DateTime getDateTime(){
        return this.dateTimeValue;
    }

    public DateTime userInputToDateTime() {
        switch (this.value.replaceAll("\\s+", "")) {
            case "nu":
            case "vandaag":
                return DateTime.now();
            case "straks":
                return DateTime.now().plusHours(2);
            case "vanochtend":
                return DateTime.now().withTimeAtStartOfDay().plusHours(7);
            case "vanmiddag":
                return DateTime.now().withTimeAtStartOfDay().plusHours(12);
            case "vanavond":
                return DateTime.now().withTimeAtStartOfDay().plusHours(18);

            case "morgenochtend":
                return DateTime.now().withTimeAtStartOfDay().plusDays(1).plusHours(7);
            case "morgen":
            case "morgenmiddag":
                return DateTime.now().withTimeAtStartOfDay().plusDays(1).plusHours(12);
            case "morgenavond":
                return DateTime.now().withTimeAtStartOfDay().plusDays(1).plusHours(18);

            case "gisterochtend":
                return DateTime.now().withTimeAtStartOfDay().minusDays(1).plusHours(7);
            case "gisteren":
            case "gistermiddag":
                return DateTime.now().withTimeAtStartOfDay().minusDays(1).plusHours(12);
            case "gisteravond":
                return DateTime.now().withTimeAtStartOfDay().minusDays(1).plusHours(18);


            case "overmorgen":
                return DateTime.now().withTimeAtStartOfDay().plusDays(2).plusHours(12);
            case "eergisteren":
                return DateTime.now().withTimeAtStartOfDay().minusDays(2);

            case "maandag":
                return DateTime.now().withDayOfWeek(DateTimeConstants.MONDAY).withTime(12,0,0,0);
            case "dinsdag":
                return DateTime.now().withDayOfWeek(DateTimeConstants.TUESDAY).withTime(12,0,0,0);
            case "woensdag":
                return DateTime.now().withDayOfWeek(DateTimeConstants.WEDNESDAY).withTime(12,0,0,0);
            case "donderdag":
                return DateTime.now().withDayOfWeek(DateTimeConstants.THURSDAY).withTime(12,0,0,0);
            case "vrijdag":
                return DateTime.now().withDayOfWeek(DateTimeConstants.FRIDAY).withTime(12,0,0,0);
            case "zaterdag":
                return DateTime.now().withDayOfWeek(DateTimeConstants.SATURDAY).withTime(12,0,0,0);
            case "zondag":
                return DateTime.now().withDayOfWeek(DateTimeConstants.SUNDAY).withTime(12,0,0,0);


            default:
                this.value = "nu";
                return DateTime.now();
        }
    }

    private String getDirection() {
        return "next";
    }

}
