package nl.jherder.modules.conversation.actions;

import com.github.dvdme.ForecastIOLib.*;
import com.google.inject.Inject;
import nl.jherder.core.util.TimeUtil;
import nl.jherder.modules.conversation.context.Context;
import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.conversation.context.DateTimeContext;
import nl.jherder.modules.conversation.context.DefaultContext;
import nl.jherder.modules.conversation.models.ConversationResponse;
import nl.jherder.modules.weather.NoForecastException;
import nl.jherder.modules.weather.WeatherService;
import org.joda.time.DateTime;

public class WeatherAction implements IAction {

    public static final String WIT_NAME = "weather";

    @Inject
    private ContextService contextService;

    @Inject
    private WeatherService weatherService;

    @Override
    public ConversationResponse run() {
        String message = execute();
        return new ConversationResponse(message);
    }

    private String execute() {
        Context location = this.contextService.getContext("location");
        DateTimeContext datetimeContext = (DateTimeContext) this.contextService.getContext("datetime");

        if (location == null) {
            location = new DefaultContext("location", "Utrecht");
        }

        if (datetimeContext == null) {
            datetimeContext = new DateTimeContext("nu");
        }

        try {
            ForecastIO forecast = weatherService.getForecast(location.getValue());
            DateTime dateTime = datetimeContext.getDateTime();

            if (TimeUtil.isWithinHours(dateTime, 1)) {
                FIODataPoint currently = new FIOCurrently(forecast).get();
                String summary = currently.summary().replace("\"", "").toLowerCase();
                return "Het is in " + location.getValue() + " is " + currently.temperature().intValue() + " \u00b0C en " + summary;
            }

            if (TimeUtil.isWithinHours(dateTime, 3)) {
                FIODataPoint hourly = new FIOHourly(forecast).getHour(3);
                String summary = hourly.summary().replace("\"", "").toLowerCase();
                return "Het is in " + location.getValue() + " straks " + hourly.temperature().intValue() + " \u00b0C en " + summary;
            }

            if (TimeUtil.isToday(dateTime)) {
                FIODataPoint daily = new FIODaily(forecast).getDay(1);
                String summary = daily.summary().replace("\"", "").toLowerCase();

                int temperatureMin = daily.temperatureMin().intValue();
                int temperatureMax = daily.temperatureMax().intValue();

                return "Het wordt vandaag in " + location.getValue() + " tussen de " + temperatureMin + " en de " + temperatureMax + " \u00b0C en ";
            }

            if (TimeUtil.isAfterToday(dateTime)) {
                int day = TimeUtil.getDaysFromNow(dateTime) + 1;
                FIODataPoint daily = new FIODaily(forecast).getDay(day);
                String summary = daily.summary().replace("\"", "").toLowerCase();

                int temperatureMin = daily.temperatureMin().intValue();
                int temperatureMax = daily.temperatureMax().intValue();

                return "Het wordt in " + location.getValue() + " " + datetimeContext.getValue() + " tussen de " + temperatureMin + " en " + temperatureMax + " \u00b0C en " + summary;
            }

            return "Ik kan nog niet zeggen wat het weer in het verleden was";

        } catch (NoForecastException e) {
            return e.getMessage();
        }
    }
}
