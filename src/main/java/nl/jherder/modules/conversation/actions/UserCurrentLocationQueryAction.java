package nl.jherder.modules.conversation.actions;

import com.google.inject.Inject;
import com.google.maps.model.GeocodingResult;
import nl.jherder.jdbi.UserEntity;
import nl.jherder.jdbi.UserLocationEntity;
import nl.jherder.modules.conversation.context.Context;
import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.conversation.models.ConversationResponse;
import nl.jherder.modules.geolocation.models.StaticMapRequest;
import nl.jherder.modules.geolocation.services.UserLocationService;
import nl.jherder.modules.user.services.UserService;

import java.time.LocalDateTime;
import java.util.List;

public class UserCurrentLocationQueryAction implements IAction {

    public static final String WIT_NAME = "user_current_location_query";

    @Inject
    private ContextService contextService;

    @Inject
    private UserService userService;

    @Inject
    private UserLocationService userLocationService;

    private ConversationResponse response;

    @Override
    public ConversationResponse run() {
        response = new ConversationResponse("Ik weet niet hoe ik je kan helpen");
        execute();


        return response;
    }

    private void execute() {
        Context intent = contextService.getContext("intent");
        Context contact = contextService.getContext("contact");

        if (contact == null) {
            intent.setLifespan(2);
            response.setMessage("Van wie wil je de locatie weten?");
            return;
        }

        List<UserEntity> userEntities = userService.findAllByName(contact.getValue());

        if (userEntities.size() == 0) {
            response.setMessage("Ik weet niet wie " + contact.getValue() + " is");
            return;
        }

        if (userEntities.size() > 1) {
            intent.setLifespan(2);
            response.setMessage("Wie bedoel je met " + contact.getValue() + ", ik ken er meerdere!");
            return;
        }

        UserEntity user = userEntities.get(0);
        UserLocationEntity lastLocation = userLocationService.getLastUserLocation(user);

        if (lastLocation == null) {
            response.setMessage("De meest recente locatie van " + user.getFirstName() + " is niet bekend");
            return;
        }

        StaticMapRequest staticMapRequest = userLocationService.getStaticMapRequest(lastLocation);
        staticMapRequest.setWidth(300);
        staticMapRequest.setHeight(200);
        response.setImageUrl(staticMapRequest.getImageUrl());

        String mapsLink = userLocationService.getGoogleMapsUrl(lastLocation);
        response.setLink(mapsLink);

        LocalDateTime locationReportTime = lastLocation.getCreationTime().toLocalDateTime();
        String readableLocationReportTime = locationReportTime.toLocalTime().toString();

        if (userLocationService.isUserAtHome(user)) {
            String message = String.format("%s is thuis (%s)", user.getFirstName(), readableLocationReportTime);
            response.setMessage(message);
            return;
        }

        if (userLocationService.isUserAtWork(user)) {
            String message = String.format("%s is op werk (%s)", user.getFirstName(), readableLocationReportTime);
            response.setMessage(message);
            return;
        }

        try {
            GeocodingResult result = userLocationService.getAddress(lastLocation);
            String message = String.format("%s (%s)", result.formattedAddress, readableLocationReportTime);
            response.setMessage(message);
        } catch (Exception e) {
            response.setMessage("Er is iets misgegaan bij het ophalen van de locatie");
        }
    }

}
