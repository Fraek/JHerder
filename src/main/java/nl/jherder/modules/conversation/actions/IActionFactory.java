package nl.jherder.modules.conversation.actions;

import nl.jherder.modules.conversation.context.Context;

public interface IActionFactory {

    IAction create(Context context);

}
