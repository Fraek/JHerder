package nl.jherder.modules.conversation.actions;

import com.google.inject.Inject;
import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.conversation.models.ConversationResponse;

import java.util.Random;

public class ThanksAction implements IAction {

    public static final String WIT_NAME = "thanks";

    private static final String[] responses = {
            "Graag gedaan \uD83D\uDE04",
            "Geen probleem \uD83D\uDE09",
            "Geen dank",
            "Alsjeblieft \uD83D\uDE0A"
    };

    @Inject
    private ContextService contextService;

    @Override
    public ConversationResponse run() {
        this.contextService.reset("intent");
        String message = responses[(new Random()).nextInt(responses.length)];
        return new ConversationResponse(message);
    }
}
