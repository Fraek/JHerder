package nl.jherder.modules.conversation.actions;

import nl.jherder.modules.conversation.models.ConversationResponse;

public interface IAction {

    /**
     * @return string response van Herder naar de gebruiker toe
     */
    ConversationResponse run();


}
