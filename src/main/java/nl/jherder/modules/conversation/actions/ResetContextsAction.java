package nl.jherder.modules.conversation.actions;

import com.google.inject.Inject;
import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.conversation.models.ConversationResponse;

public class ResetContextsAction implements IAction{

    public static final String WIT_NAME = "reset_all";

    @Inject
    private ContextService contextService;

    @Override
    public ConversationResponse run() {
       this.contextService.reset();
       String message =  "Oké, context gereset";
       return new ConversationResponse(message);
    }

}
