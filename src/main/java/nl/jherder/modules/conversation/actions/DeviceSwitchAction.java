package nl.jherder.modules.conversation.actions;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import nl.jherder.jdbi.DeviceEntity;
import nl.jherder.modules.conversation.context.Context;
import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.conversation.models.ConversationResponse;
import nl.jherder.modules.domotica.queue.DeviceCommander;
import nl.jherder.modules.domotica.services.DeviceService;

import java.util.*;

public class DeviceSwitchAction implements IAction {

    public static final String WIT_NAME = "switch_device";

    private Context zone;
    private Context powerSwitch;

    @Inject
    private ContextService contextService;

    @Inject
    private DeviceCommander deviceCommander;

    @Override
    public ConversationResponse run() {
        String message =  execute();
        return new ConversationResponse(message);
    }

    private String execute() {
        Context intent = contextService.getContext("intent");
        this.zone = contextService.getContext("zone");
        this.powerSwitch = contextService.getContext("power_switch");
        Context deviceName = contextService.getContext("device");

        DeviceService deviceService = deviceCommander.getDeviceService();

        if (deviceName == null) {
            intent.setLifespan(2);
            return "Welk apparaat?";
        }

        if (this.powerSwitch == null) {
            intent.setLifespan(2);
            deviceName.setLifespan(2);
            return "Aan of uit?";
        }

        List<DeviceEntity> deviceEntities = deviceService.findAllByName(deviceName.getValue());

        if (deviceEntities.size() == 0) {
            intent.setLifespan(2);
            this.powerSwitch.setLifespan(2);
            return "Sorry ik kan " + deviceName.getValue() + " niet vinden";
        }

        if (deviceEntities.size() > 1) {
            Iterable<DeviceEntity> sameZoneDevices = Iterables.filter(deviceEntities, device -> device != null && device.getZone().getId() == deviceEntities.get(0).getZone().getId());
            if (deviceEntities.size() == Iterables.size(sameZoneDevices)) {
                this.switchDevices(deviceEntities);
                return "Oké, " + this.powerSwitch.getValue();
            }

            if (this.zone != null) {

                List<DeviceEntity> zoneDeviceList = deviceEntities;
                if(!this.zone.getValue().equals("huis")) {

                    Iterable<DeviceEntity> zoneDevices = Iterables.filter(deviceEntities, device -> device != null && device.getZone().getName().equalsIgnoreCase(this.zone.getValue()));
                    zoneDeviceList = Lists.newArrayList(zoneDevices);
                }

                if (zoneDeviceList.size() > 0) {
                    this.switchDevices(zoneDeviceList);
                    return "Oké, " + this.powerSwitch.getValue();
                }

                intent.setLifespan(2);
                this.powerSwitch.setLifespan(2);

                return "Vreemd... volgens mij is er geen " + deviceName.getValue() + " in de " + this.zone.getValue();
            }

            intent.setLifespan(2);
            deviceName.setLifespan(2);
            this.powerSwitch.setLifespan(2);

            return "In welke ruimte wil je de " + deviceName.getValue() + " " + this.powerSwitch.getValue() + "?";
        }


        DeviceEntity device = deviceEntities.get(0);
        this.switchDevice(device);

        return "Oké ik doe de " + device.getName() + " " + this.powerSwitch.getValue();
    }


    private void switchDevice(DeviceEntity device) {
        if (this.powerSwitch.getValue().equals("aan")) {
            deviceCommander.TurnOn(device);
        } else {
            deviceCommander.TurnOff(device);
        }
    }

    private void switchDevices(List<DeviceEntity> deviceList) {
        if (this.powerSwitch.getValue().equals("aan")) {
            deviceCommander.TurnOn(deviceList);
        } else {
            deviceCommander.TurnOff(deviceList);
        }
    }
}
