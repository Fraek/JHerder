package nl.jherder.modules.conversation.actions;

import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.conversation.models.ConversationResponse;

import java.util.Random;

public class JokeAction implements IAction{

    public static final String WIT_NAME = "joke";

    private static final String[] responses = {
            "Computers: de meeste fouten zitten tussen het toetsenbord en de stoel. \uD83D\uDE02",
            "Why do Java developers wear glasses? - Because they don't C# \uD83D\uDE06"
    };

    @Override
    public ConversationResponse run() {
        String message =  responses[(new Random()).nextInt(responses.length)];
        return new ConversationResponse(message);
    }
}
