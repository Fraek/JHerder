package nl.jherder.modules.conversation.actions;

import com.google.inject.Inject;
import nl.jherder.jdbi.DeviceEntity;
import nl.jherder.modules.conversation.context.Context;
import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.conversation.models.ConversationResponse;
import nl.jherder.modules.domotica.queue.DeviceCommander;
import nl.jherder.modules.domotica.services.DeviceService;

import java.util.List;

public class LightsAction implements IAction {

    public static final String WIT_NAME = "lights";

    private Context zone;
    private Context intent;
    private Context powerSwitch;

    @Inject
    private ContextService contextService;

    @Inject
    private DeviceCommander deviceCommander;

    @Override
    public ConversationResponse run() {

        String message = execute();
        return new ConversationResponse(message);
    }

    private String execute() {
        this.intent = contextService.getContext("intent");
        this.zone = contextService.getContext("zone");
        this.powerSwitch = contextService.getContext("power_switch");

        if (this.isReady()) {
            try {
                switchLamps();
                return "Oké ik doe de lampen in de " + this.zone.getValue() + " " + this.powerSwitch.getValue();
            } catch (Exception e) {
                return "Het is mislukt de lampen in de " + this.zone.getValue() + " " + this.powerSwitch.getValue() + " te doen";
            }
        }

        this.intent.setLifespan(2);

        if (this.powerSwitch != null) {
            this.powerSwitch.setLifespan(2);
        }

        if (this.zone == null) {
            return "In welke ruimte?";
        }

        if (this.powerSwitch == null) {
            this.zone.setLifespan(2);
            return "Moet ik de lampen in de " + this.zone.getValue() + " aan of uit doen?";
        }

        return "Sorry ik heb geen idee hoe ik je kan helpen";
    }

    private void switchLamps() {
        DeviceService deviceService = deviceCommander.getDeviceService();

        List<DeviceEntity> deviceEntities;
        if (this.zone.getValue().equals("huis")) {
            deviceEntities = deviceService.findAllByType("Light");
        } else {
            deviceEntities = deviceService.findAllByTypeAndZone("Light", this.zone.getValue());
        }

        if (this.powerSwitch.getValue().equals("aan")) {
            deviceCommander.TurnOn(deviceEntities);
        } else {
            deviceCommander.TurnOff(deviceEntities);
        }
    }

    private boolean isReady() {
        return this.zone != null && this.intent != null && this.powerSwitch != null;
    }
}
