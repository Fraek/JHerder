package nl.jherder.modules.conversation.actions;

public interface IContextDependable {

    boolean isReady();

}
