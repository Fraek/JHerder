package nl.jherder.modules.conversation.actions;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import nl.jherder.Application;
import nl.jherder.core.bundles.GuiceJobsBundle;
import nl.jherder.modules.conversation.context.Context;
import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.domotica.queue.DeviceCommander;
import nl.jherder.modules.domotica.services.DomoticzService;
import nl.jherder.modules.geolocation.services.UserLocationService;
import nl.jherder.modules.user.services.UserService;
import nl.jherder.modules.weather.WeatherService;

public class ActionFactory implements IActionFactory {

    @Override
    public IAction create(Context context) {

        Injector injector = Application.getInjector();

        switch (context.getValue()) {
            case GreetingAction.WIT_NAME:
                return injector.getInstance(GreetingAction.class);
            case ThanksAction.WIT_NAME:
                return injector.getInstance(ThanksAction.class);
            case LightsAction.WIT_NAME:
                return injector.getInstance(LightsAction.class);
            case DeviceSwitchAction.WIT_NAME:
                return injector.getInstance(DeviceSwitchAction.class);
            case DeviceSwitchAllAction.WIT_NAME:
                return injector.getInstance(DeviceSwitchAllAction.class);
            case WeatherAction.WIT_NAME:
                return injector.getInstance(WeatherAction.class);
            case JokeAction.WIT_NAME:
                return injector.getInstance(JokeAction.class);
            case ResetContextsAction.WIT_NAME:
                return injector.getInstance(ResetContextsAction.class);
            case UserCurrentLocationQueryAction.WIT_NAME:
                return injector.getInstance(UserCurrentLocationQueryAction.class);
            default:
                return null;
        }
    }
}
