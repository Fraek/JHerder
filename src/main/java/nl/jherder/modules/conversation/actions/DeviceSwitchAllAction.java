package nl.jherder.modules.conversation.actions;

import com.google.inject.Inject;
import nl.jherder.jdbi.DeviceEntity;
import nl.jherder.modules.conversation.context.Context;
import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.conversation.models.ConversationResponse;
import nl.jherder.modules.domotica.queue.DeviceCommander;

import java.util.List;

public class DeviceSwitchAllAction implements IAction {

    public static final String WIT_NAME = "switch_all_devices";

    private Context powerSwitch;

    @Inject
    private ContextService contextService;

    @Inject
    private DeviceCommander deviceCommander;

    @Override
    public ConversationResponse run() {

        powerSwitch = contextService.getContext("power_switch");

        if (powerSwitch == null) {
            return new ConversationResponse("Moet ik alles aan of uit doen?");
        }

        List<DeviceEntity> deviceList = deviceCommander.getDeviceService().findAll();
        return switchDevices(deviceList);
    }

    private ConversationResponse switchDevices(List<DeviceEntity> deviceList) {
        if (this.powerSwitch.getValue().equals("aan")) {
            deviceCommander.TurnOn(deviceList);
            return new ConversationResponse("Ik heb alles aan gedaan");
        } else {
            deviceCommander.TurnOff(deviceList);
            return new ConversationResponse("Ik heb alles uit gedaan");
        }
    }

}
