package nl.jherder.modules.conversation.actions;

import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.conversation.models.ConversationResponse;

import java.util.Random;

public class GreetingAction implements IAction {

    public static final String WIT_NAME = "greeting";

    private static final String[] responses = {
            "Hoi", "Hey", "Hallo", "Hi"
    };

    @Override
    public ConversationResponse run() {
        String message = responses[(new Random()).nextInt(responses.length)];
        return new ConversationResponse(message);
    }
}