package nl.jherder.modules.conversation.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class WitResponse {
    @JsonProperty("msg_id")
    public String messageId;
    @JsonProperty("_text")
    public String text;
    @JsonProperty("entities")
    public Map<String, WitEntity[]> entities;

    public WitEntity[] GetByName(String name) {
        return entities.get(name);
    }
}