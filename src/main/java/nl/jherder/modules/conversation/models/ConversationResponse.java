package nl.jherder.modules.conversation.models;

public class ConversationResponse {

    private String message;
    private String imageUrl;
    private String link;

    // todo quick answers


    public ConversationResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
