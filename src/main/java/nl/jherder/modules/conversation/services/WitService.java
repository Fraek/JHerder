package nl.jherder.modules.conversation.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import nl.jherder.modules.conversation.models.WitResponse;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.URISyntaxException;

public class WitService {

    private static final String baseUrl = "https://api.wit.ai";
    private static final String AccessToken = "DCTAIFMEEM3SI7KAHCW7UL64MAEESZ5G";

    @Inject
    private HttpClient httpClient;

    public WitResponse sendMessage(String message) {
        URIBuilder builder = null;
        try {
            builder = new URIBuilder(baseUrl + "/message");
            builder.addParameter("q", message);
            HttpGet request = new HttpGet(builder.build());
            request.addHeader("Authorization", "Bearer " + AccessToken);
            HttpResponse response = this.httpClient.execute(request);
            return (new ObjectMapper()).readValue(response.getEntity().getContent(), WitResponse.class);
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
