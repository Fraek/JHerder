package nl.jherder.modules.conversation.services;

import com.google.inject.Inject;
import nl.jherder.modules.conversation.actions.ActionFactory;
import nl.jherder.modules.conversation.actions.IAction;
import nl.jherder.modules.conversation.context.Context;
import nl.jherder.modules.conversation.context.ContextFactory;
import nl.jherder.modules.conversation.context.ContextService;
import nl.jherder.modules.conversation.models.ConversationResponse;
import nl.jherder.modules.conversation.models.WitEntity;
import nl.jherder.modules.conversation.models.WitResponse;

import java.util.Map;

public class Interpreter {

    @Inject
    private ContextService contextService;
    @Inject
    private ActionFactory actionFactory;

    public ConversationResponse run(WitResponse witResponse) {
        this.updateContexts(witResponse);

        ConversationResponse response = this.interpret();
        this.contextService.spend();
        return response;
    }

    private ConversationResponse interpret() {
        Context intentContext = contextService.getContext("intent");

        if (intentContext == null) {
            return handleUnknownIntent();
        }

        IAction intendedAction = actionFactory.create(intentContext);
        if (intendedAction == null) {
            return handleUnknownAction();
        }

        try {
            return intendedAction.run();
        } catch (Exception e) {
            return new ConversationResponse(e.getMessage());
        }
    }

    private ConversationResponse handleUnknownAction() {
        contextService.reset("intent");
        String message = "Ik begrijp wat je bedoelt, maar ik weet niet wat ik moet doen \uD83D\uDE15";
        return new ConversationResponse(message);
    }

    private ConversationResponse handleUnknownIntent() {
        String message = "Sorry, ik begrijp niet wat je bedoelt \uD83D\uDE27";
        return new ConversationResponse(message);
    }

    private void updateContexts(WitResponse response) {

        for (Map.Entry<String, WitEntity[]> entry : response.entities.entrySet()) {
            String name = entry.getKey();
            WitEntity entities[] = entry.getValue();

            Context context = ContextFactory.create(name, entities[0]);
            contextService.setContext(context);
        }

        contextService.evaluate();
    }
}
