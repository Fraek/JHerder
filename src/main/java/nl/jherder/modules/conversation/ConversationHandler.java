package nl.jherder.modules.conversation;

import nl.jherder.modules.conversation.services.BlaService;
import org.atmosphere.config.service.AtmosphereHandlerService;
import org.atmosphere.cpr.*;

import javax.inject.Inject;
import java.io.IOException;

@AtmosphereHandlerService
public class ConversationHandler implements AtmosphereHandler {

    public BlaService blaService = new BlaService();

    @Override
    public void onRequest(AtmosphereResource atmosphereResource) throws IOException {
        Broadcaster broadcaster = atmosphereResource.getBroadcaster();
        atmosphereResource.setBroadcaster(broadcaster);
        atmosphereResource.suspend();

        String msg = atmosphereResource.getRequest().body().asString();
        String response = blaService.InterpretMessage(msg);
        atmosphereResource.write(response);
    }

    private boolean isBroadcast(AtmosphereResourceEvent event) {
        return event.getMessage() != null && !event.isCancelled() && !event.isClosedByClient() && !event.isClosedByApplication();
    }

    @Override
    public void onStateChange(AtmosphereResourceEvent event) throws IOException {
        AtmosphereResource resource = event.getResource();

        if (isBroadcast(event)) {
            resource.write(event.getMessage().toString());

            switch (resource.transport()) {
                case WEBSOCKET:
                case STREAMING:
                    resource.getResponse().flushBuffer();
                    break;
                default:
                    resource.resume();
                    break;
            }
        }
    }

    @Override
    public void destroy() {
    }

}
