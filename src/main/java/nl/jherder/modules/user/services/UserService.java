package nl.jherder.modules.user.services;

import nl.jherder.core.service.AbstractService;
import nl.jherder.jdbi.UserEntity;
import nl.jherder.jdbi.UserWaypointsEntity;
import nl.jherder.jdbi.WaypointEntity;
import org.hibernate.SessionFactory;

import javax.inject.Inject;
import java.util.List;

public class UserService extends AbstractService<UserEntity> {

    @Inject
    public UserService(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<UserEntity> findAllByName(String name){
        return list(namedQuery("UserEntity.findAllByName")
                .setParameter("userName", "%" + name + "%"));
    }



}
