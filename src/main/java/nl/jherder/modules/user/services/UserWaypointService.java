package nl.jherder.modules.user.services;

import nl.jherder.core.service.AbstractService;
import nl.jherder.jdbi.UserEntity;
import nl.jherder.jdbi.UserWaypointsEntity;
import nl.jherder.jdbi.WaypointEntity;
import org.hibernate.SessionFactory;

import javax.inject.Inject;
import java.util.List;

public class UserWaypointService extends AbstractService<UserWaypointsEntity> {

    @Inject
    public UserWaypointService(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public UserWaypointsEntity getUserWaypoints(UserEntity user){
        List<UserWaypointsEntity> userWaypointsEntities = list(namedQuery("UserWaypointsEntity.findByUserId").setParameter("user", user));
        if(userWaypointsEntities.size() > 0){
            return  userWaypointsEntities.get(0);
        }

        return null;
    }

    public WaypointEntity getUserHomeWaypoint(UserEntity user){
        UserWaypointsEntity userWaypointsEntity = getUserWaypoints(user);
        if(userWaypointsEntity == null){
            return null;
        }

        return userWaypointsEntity.getHomeByHomeId();
    }

    public WaypointEntity getUserWorkWaypoint(UserEntity user){
        UserWaypointsEntity userWaypointsEntity = getUserWaypoints(user);
        if(userWaypointsEntity == null){
            return null;
        }

        return userWaypointsEntity.getWorkByWorkId();
    }


}
