package nl.jherder.modules.weather;

public class NoForecastException extends Exception {

    public NoForecastException(String message) {
        super(message);
    }
}
