package nl.jherder.modules.weather;

import com.github.dvdme.ForecastIOLib.*;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

import javax.inject.Inject;

public class WeatherService {

    private static final String API_KEY = "e1c4becbf0d8f1e16ee1d4acac4bf082";

    @Inject
    private GeoApiContext geoApiContext;

    public ForecastIO getForecast(String city) throws NoForecastException {
        ForecastIO fio = new ForecastIO(API_KEY);
        fio.setLang(ForecastIO.LANG_DUTCH);
        fio.setUnits(ForecastIO.UNITS_SI);

        try {
            GeocodingResult[] results = GeocodingApi.geocode(geoApiContext, city).await();
            LatLng location = results[0].geometry.location;
            fio.getForecast(String.valueOf(location.lat), String.valueOf(location.lng));

            return fio;

        } catch (Exception e) {
            throw new NoForecastException("Ik kan geen weerbericht vinden voor " + city);
        }

    }
}