package nl.jherder.modules.domotica.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import nl.jherder.jdbi.DeviceType;
import nl.jherder.modules.domotica.services.DeviceTypeService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/device-type")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DeviceTypeResource {

    @Inject
    private DeviceTypeService service;

    @GET
    @UnitOfWork
    public List<DeviceType> listDevices() {
        return service.findAll();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{id}")
    public DeviceType findDeviceType(@PathParam("id") LongParam id){
        return service.findById(id.get());
    }

    @POST
    @UnitOfWork
    public Response addDeviceType(@NotNull @Valid DeviceType deviceType){
        service.save(deviceType);
        return Response.status(Response.Status.CREATED).entity(deviceType).build();
    }

    @PUT
    @Timed
    @UnitOfWork
    @Path("/{id}")
    public Response updateDevice(@PathParam("id") LongParam id, @NotNull @Valid DeviceType deviceType) {
        DeviceType oldDeviceType = service.findById(id.get());
        if(oldDeviceType == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        oldDeviceType.update(deviceType);
        deviceType = service.save(oldDeviceType);
        return Response.ok().entity(deviceType).build();
    }

    @DELETE
    @UnitOfWork
    @Path("/{id}")
    public Response deleteDevice(@PathParam("id") LongParam id) {
        DeviceType deviceType = service.findById(id.get());
        if(deviceType == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        service.delete(deviceType);
        return Response.noContent().build();
    }

}
