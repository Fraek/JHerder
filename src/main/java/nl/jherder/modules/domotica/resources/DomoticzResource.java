package nl.jherder.modules.domotica.resources;

import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import nl.jherder.jdbi.DeviceEntity;
import nl.jherder.modules.automation.commands.domoticz.DomoticzService;
import nl.jherder.modules.domotica.models.DomoticzDevice;
import nl.jherder.modules.domotica.services.DeviceService;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("/domoticz")
@Produces(MediaType.APPLICATION_JSON)
public class DomoticzResource {

    @Inject
    private DomoticzService domoticzService;

    @Inject
    private DeviceService deviceService;

    @GET
    public String index() {
        return "Domoticz";
    }

    @GET
    @UnitOfWork
    @Path("/status/{idx}")
    public String getStatus(@PathParam("idx") String idx) throws Exception {
        return domoticzService.getDeviceStatus(idx);
    }

    @GET
    @UnitOfWork
    @Path("/discover")
    public List<DomoticzDevice> discoverDevices() throws Exception {
        List<DomoticzDevice> newDevices = new ArrayList<>();
        List<String> knownReferences = new ArrayList<>();

        for (DeviceEntity device : deviceService.findAll())
            knownReferences.add(device.getReference());

        for (DomoticzDevice domDevice : domoticzService.getDevices()) {
            String domoticzReference = domDevice.idx;
            if (knownReferences.contains(domoticzReference))
                continue;

            newDevices.add(domDevice);
            knownReferences.add(domoticzReference);
        }
        return newDevices;
    }
}