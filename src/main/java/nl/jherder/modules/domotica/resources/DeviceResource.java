package nl.jherder.modules.domotica.resources;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonView;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.IntParam;
import nl.jherder.jdbi.DeviceEntity;
import nl.jherder.jdbi.DeviceType;
import nl.jherder.modules.automation.instructions.OffInstruction;
import nl.jherder.modules.automation.instructions.OnInstruction;
import nl.jherder.modules.automation.services.CommandService;
import nl.jherder.modules.domotica.models.DeviceAddModel;
import nl.jherder.modules.domotica.models.DeviceModel;
import nl.jherder.modules.domotica.models.views.DeviceView;
import nl.jherder.jdbi.Device;
import nl.jherder.modules.domotica.services.DeviceService;
import nl.jherder.modules.domotica.services.DeviceTypeService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Path("/device")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DeviceResource {

    @Inject
    private DeviceService deviceService;
    @Inject
    private DeviceTypeService deviceTypeService;
    @Inject
    private CommandService commandService;
    @Inject
    private ModelMapper modelMapper;

    @GET
    @UnitOfWork
    @JsonView(DeviceView.List.class)
    public List<DeviceModel> listDevices() {
        List<DeviceEntity> devices = deviceService.findAll();
        Type listType = new TypeToken<List<DeviceModel>>(){}.getType();

        return modelMapper.map(devices, listType);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @JsonView(DeviceView.Detail.class)
    public DeviceModel findDevice(@PathParam("id") IntParam id) {
        DeviceEntity device = deviceService.findById(id.get());
        return modelMapper.map(device, DeviceModel.class);
    }

    @POST
    @UnitOfWork
    @JsonView(DeviceView.Detail.class)
    public Response addDevice(@NotNull @Valid DeviceAddModel device) {
        deviceService.save(device);
        return Response.status(Response.Status.CREATED).entity(device).build();
    }

    @PUT
    @Timed
    @UnitOfWork
    @Path("/{id}/command")
    public Response updateDevice(@PathParam("id") IntParam id, @QueryParam("turn") String turn) {
        DeviceEntity device = deviceService.findById(id.get());
        if (device == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        switch (turn.toLowerCase()) {
            case "on":
                commandService.execute(new OnInstruction(device));

                //Update device status
                device.setStatus(1);
                deviceService.save(device);

                return Response.ok().build();
            case "off":
                commandService.execute(new OffInstruction(device));

                //Update device status
                device.setStatus(2);
                deviceService.save(device);

                return Response.ok().build();
            default:
                return Response.status(Response.Status.BAD_REQUEST).entity("error").build();
        }
    }

    @PUT
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @JsonView(DeviceView.Detail.class)
    public Response updateDevice(@PathParam("id") IntParam id, @NotNull @Valid DeviceModel device) {
        DeviceEntity oldDevice = deviceService.findById(id.get());
        if (oldDevice == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        oldDevice.setName(device.name != null ? device.name : oldDevice.getName());
        oldDevice.setDescription(device.description != null ? device.description : oldDevice.getDescription());
//        oldDevice.setType(device.type != null ? device.ty: oldDevice.getType());
//        Long deviceTypeId = device.getDeviceTypeId();
//        if (deviceTypeId != null) {
//            DeviceType deviceType = deviceTypeService.findById(device.getDeviceTypeId());
//            oldDevice.setDeviceType(deviceType);
//        }

        DeviceEntity savedDevice = deviceService.save(oldDevice);
        return Response.ok().entity(modelMapper.map(savedDevice, DeviceModel.class)).build();
    }

    @DELETE
    @UnitOfWork
    @Path("/{id}")
    public Response deleteDevice(@PathParam("id") IntParam id) {
        DeviceEntity device = deviceService.findById(id.get());
        if (device == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        deviceService.delete(device);
        return Response.noContent().build();
    }

    @GET
    @UnitOfWork
    @JsonView(DeviceView.List.class)
    @Path("/list")
    public List<DeviceModel> listByDeviceType(@QueryParam("type") String type) {
        List<DeviceEntity> devices = deviceService.findAllByType(type);
        Type listType = new TypeToken<List<DeviceModel>>(){}.getType();

        return modelMapper.map(devices, listType);
    }

}