package nl.jherder.modules.domotica.resources;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonView;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import nl.jherder.modules.domotica.models.views.ZoneView;
import nl.jherder.jdbi.Zone;
import nl.jherder.modules.domotica.services.ZoneService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/zone")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ZoneResource {

    @Inject
    private ZoneService zoneService;

    @GET
    @JsonView(ZoneView.List.class)
    @UnitOfWork
    public List<Zone> listZones() {
        return zoneService.findAll();
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @JsonView(ZoneView.Detail.class)
    public Zone findDevice(@PathParam("id") LongParam id) {
        return zoneService.findById(id.get());
    }

}
