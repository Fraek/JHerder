package nl.jherder.modules.domotica.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DomoticzResponse {

    @JsonProperty
    public String ActTime;
    @JsonProperty
    public String status;
    @JsonProperty
    public String title;
    @JsonProperty("result")
    private List<DomoticzDevice> devices;

    public List<DomoticzDevice> getDevices() {
        return this.devices;
    }

}