package nl.jherder.modules.domotica.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import nl.jherder.core.converters.TypeToIntConverter;
import nl.jherder.core.mapping.IMap;
import nl.jherder.jdbi.DeviceEntity;
import nl.jherder.modules.domotica.defines.Type;
import org.modelmapper.PropertyMap;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceAddModel implements IMap<DeviceAddModel, DeviceEntity> {

    public String name;
    public String description;
    public int reference;
    public Type type;
    public ZoneModel zone;
    public DeviceTypeModel deviceType;

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public PropertyMap<DeviceAddModel, DeviceEntity> getMapping() {

        return new PropertyMap<DeviceAddModel, DeviceEntity>() {
            protected void configure() {
                using(new TypeToIntConverter()).map(source.type).setType(0);
            }
        };
    }
}
