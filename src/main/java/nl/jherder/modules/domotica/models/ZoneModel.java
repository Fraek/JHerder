package nl.jherder.modules.domotica.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ZoneModel {

    public int id;
    public String name;

}
