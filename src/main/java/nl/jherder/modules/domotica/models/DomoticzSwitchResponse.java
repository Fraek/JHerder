package nl.jherder.modules.domotica.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DomoticzSwitchResponse {

    @JsonProperty
    public String status;
    @JsonProperty
    public String title;
    @JsonProperty
    public String message;

    public boolean wasSuccessful(){
        return this.status.toLowerCase().equals("ok");
    }

}