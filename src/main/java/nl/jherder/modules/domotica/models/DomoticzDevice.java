package nl.jherder.modules.domotica.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value="result")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DomoticzDevice {

    @JsonProperty("idx")
    public String idx = null;
    @JsonProperty("Name")
    public String name = null;
    @JsonProperty("Status")
    public String status = null;
    @JsonProperty("Data")
    public String data = null;
    @JsonProperty("Description")
    public String description = null;
    @JsonProperty("ID")
    public String ID = null;
    @JsonProperty("Type")
    public String type = null;
    @JsonProperty("SubType")
    public String subType = null;

}