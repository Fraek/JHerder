package nl.jherder.modules.domotica.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import nl.jherder.core.converters.IntToStatusConverter;
import nl.jherder.core.mapping.IMap;
import nl.jherder.jdbi.DeviceEntity;
import nl.jherder.modules.domotica.defines.Status;
import org.modelmapper.PropertyMap;

import java.sql.Timestamp;

public class DeviceModel implements IMap<DeviceEntity, DeviceModel> {

    public int id;
    public String name;
    public String description;
    public String reference;
    public Status status;
    public Timestamp updateTime;
    public DeviceTypeModel deviceType;
    public ZoneModel zone;

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    @JsonIgnore
    public PropertyMap<DeviceEntity, DeviceModel> getMapping() {
        return new PropertyMap<DeviceEntity, DeviceModel>() {
            @Override
            protected void configure() {
                using(new IntToStatusConverter()).map(source.getStatus()).setStatus(Status.UNKNOWN);
            }
        };
    }
}
