package nl.jherder.modules.domotica.models.views;

public class DeviceView {
    public static class List {}
    public static class Minimal {}
    public static class Detail extends Minimal {}
}
