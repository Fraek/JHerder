package nl.jherder.modules.domotica.models.views;

public class ZoneView {
    public static class List {}
    public static class Minimal {}
    public static class Detail extends Minimal {}
}
