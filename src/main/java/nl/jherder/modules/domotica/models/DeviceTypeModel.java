package nl.jherder.modules.domotica.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceTypeModel {

    public int id;
    public String name;

}
