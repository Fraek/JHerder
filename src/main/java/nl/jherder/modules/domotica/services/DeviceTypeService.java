package nl.jherder.modules.domotica.services;

import com.google.inject.Inject;
import io.dropwizard.hibernate.AbstractDAO;
import nl.jherder.jdbi.DeviceType;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;

import javax.validation.constraints.NotNull;
import java.util.List;

public class DeviceTypeService extends AbstractDAO<DeviceType> {

    @Inject
    public DeviceTypeService(final SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public DeviceType findById(Long id) {
        return get(id);
    }

    public List<DeviceType> findAll() {
        return list(namedQuery("DeviceType.findAll"));
    }

    public DeviceType save(@NotNull DeviceType deviceType) {
        deviceType.setUpdatedAt(new DateTime());
        return this.persist(deviceType);
    }

    public void delete(DeviceType device) {
        this.currentSession().delete(device);
    }
}
