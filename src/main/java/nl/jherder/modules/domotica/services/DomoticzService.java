package nl.jherder.modules.domotica.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import nl.jherder.modules.domotica.models.DomoticzDevice;
import nl.jherder.modules.domotica.models.DomoticzResponse;
import nl.jherder.modules.domotica.models.DomoticzSwitchResponse;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import java.io.IOException;
import java.util.List;

public class DomoticzService {

    private String baseUrl = "http://192.168.178.178:8080/json.htm";

    @Inject
    private HttpClient httpClient;

    public List<DomoticzDevice> getDevices() throws Exception {
        HttpGet request = new HttpGet(baseUrl + "?type=devices");
        DomoticzResponse domResponse = this.getDomoticzResponse(request);
        return domResponse.getDevices();
    }

    public DomoticzDevice getDevice(String idx) throws Exception {
        HttpGet request = new HttpGet(baseUrl + "?type=devices&rid=" + idx);
        DomoticzResponse domResponse = this.getDomoticzResponse(request);

        if (domResponse.getDevices().size() < 1) {
            throw new Exception("No results found!");
        }
        return domResponse.getDevices().get(0);
    }

    public String getDeviceStatus(String idx) throws Exception {
        DomoticzDevice domDevice = this.getDevice(idx);
        return domDevice.status;
    }

    public boolean switchOn(String idx) {
        return this.switchCommand(idx, "On");
    }

    public boolean switchOff(String idx) {
        return this.switchCommand(idx, "Off");
    }

    private boolean switchCommand(String idx, String command) {
        try {
            HttpGet request = new HttpGet(baseUrl + "?type=command&param=switchlight&idx=" + idx + "&switchcmd=" + command);
            DomoticzSwitchResponse response = getDomoticzSwitchResponse(request);
            return response.wasSuccessful();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private DomoticzSwitchResponse getDomoticzSwitchResponse(HttpRequestBase request) throws IOException {
        HttpResponse httpResponse = httpClient.execute(request);
        return (new ObjectMapper()).readValue(httpResponse.getEntity().getContent(), DomoticzSwitchResponse.class);
    }

    private DomoticzResponse getDomoticzResponse(HttpRequestBase request) throws IOException {
        HttpResponse response = this.httpClient.execute(request);
        return (new ObjectMapper()).readValue(response.getEntity().getContent(), DomoticzResponse.class);
    }

}