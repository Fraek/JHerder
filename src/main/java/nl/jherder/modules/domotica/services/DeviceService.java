package nl.jherder.modules.domotica.services;

import com.google.inject.Inject;
import io.dropwizard.hibernate.AbstractDAO;
import nl.jherder.jdbi.DeviceEntity;
import nl.jherder.jdbi.DeviceTypeEntity;
import nl.jherder.jdbi.ZoneEntity;
import nl.jherder.modules.domotica.models.DeviceAddModel;
import org.hibernate.SessionFactory;
import org.modelmapper.ModelMapper;

import java.util.List;

public class DeviceService extends AbstractDAO<DeviceEntity> {

    @Inject
    private ModelMapper modelMapper;

    @Inject
    public DeviceService(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<DeviceEntity> findAll() {
        return list(namedQuery("DeviceEntity.findAll"));
    }

    public DeviceEntity findById(int id) {
        return get(id);
    }

    public void delete(DeviceEntity deviceEntity) {
        this.currentSession().delete(deviceEntity);
    }

    public List<DeviceEntity> findAllByName(String deviceName) {
        return list(namedQuery("DeviceEntity.findAllByName")
                .setParameter("deviceName", "%" + deviceName + "%"));
    }

    public List<DeviceEntity> findAllByType(String typeName) {
        return list(namedQuery("DeviceEntity.findAllByType")
                .setParameter("typeName", typeName));
    }

    public List<DeviceEntity> findAllByTypeAndZone(String typeName, String zoneName) {
        return list(namedQuery("DeviceEntity.findAllByTypeAndZone")
                .setParameter("typeName", typeName)
                .setParameter("zoneName", zoneName));
    }

    public DeviceEntity save(DeviceEntity deviceEntity) {
        return this.persist(deviceEntity);
    }

    public DeviceEntity save(DeviceAddModel addModel) {
        DeviceTypeEntity deviceTypeEntity = modelMapper.map(addModel.deviceType, DeviceTypeEntity.class);
        ZoneEntity zoneEntity = modelMapper.map(addModel.zone, ZoneEntity.class);
        DeviceEntity deviceEntity = modelMapper.map(addModel, DeviceEntity.class);

        deviceEntity.setDeviceType(deviceTypeEntity);
        deviceEntity.setZone(zoneEntity);
        return save(deviceEntity);
    }
}