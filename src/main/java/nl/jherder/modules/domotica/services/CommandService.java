package nl.jherder.modules.domotica.services;

import com.google.inject.Inject;
import nl.jherder.jdbi.Device;

public class CommandService {

    @Inject
    DomoticzService domoticzDevice;
    @Inject
    OldDeviceService oldDeviceService;

    public Device turnOff(Device device){
        String idx = String.valueOf(device.reference);
        if(domoticzDevice.switchOff(idx)){
            device.status = 1;
        }
        return oldDeviceService.save(device);
    }

    public Device turnOn(Device device){
        String idx = String.valueOf(device.reference);
        if(domoticzDevice.switchOn(idx)){
            device.status = 2;
        }
        return oldDeviceService.save(device);
    }

}