package nl.jherder.modules.domotica.services;

import io.dropwizard.hibernate.AbstractDAO;
import nl.jherder.jdbi.Zone;
import org.hibernate.SessionFactory;

import javax.inject.Inject;
import java.util.List;

public class ZoneService extends AbstractDAO<Zone> {

    @Inject
    public ZoneService(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Zone findById(Long id){
        return get(id);
    }

    public List<Zone> findAll(){
        return list(namedQuery("Zone.findAll"));
    }
}
