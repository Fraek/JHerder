package nl.jherder.modules.domotica.services;

import com.google.inject.Inject;
import io.dropwizard.hibernate.AbstractDAO;
import nl.jherder.jdbi.Device;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;

import javax.validation.constraints.NotNull;
import java.util.List;

@Deprecated
public class OldDeviceService extends AbstractDAO<Device> {

    @Inject
    public OldDeviceService(final SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Device findById(Long id){
        return get(id);
    }

    public List<Device> findAll(){
        return list(namedQuery("DeviceModel.findAll"));
    }

    public Device save(@NotNull Device device){
        device.setUpdatedTime(new DateTime());
        return this.persist(device);
    }

    public List<Device> findAllByName(String deviceName){
        return list(namedQuery("DeviceModel.findAllByName").setParameter("deviceName", deviceName));
    }

    public List<Device> findAllByType(String typeName){
        return list(namedQuery("DeviceModel.findAllByType").setParameter("typeName", typeName));
    }

    public List<Device> findAllByTypeAndZone(String typeName, String zoneName){
        return list(namedQuery("DeviceModel.findAllByTypeAndZone").setParameter("typeName", typeName).setParameter("zoneName", zoneName));
    }

    public void delete(Device device){
        this.currentSession().delete(device);
    }
}
