package nl.jherder.modules.domotica.queue;

import com.google.inject.Inject;
import nl.jherder.jdbi.DeviceEntity;
import nl.jherder.modules.domotica.services.DeviceService;
import nl.jherder.modules.domotica.services.DomoticzService;

import java.util.List;

public class DeviceCommander {

    @Inject
    private DomoticzService domoticzService;
    @Inject
    private QueueThreadFactory queueThreadFactory;
    @Inject
    private DeviceService deviceService;

    public DeviceService getDeviceService(){
        return this.deviceService;
    }

    public void TurnOff(List<DeviceEntity> deviceList) {
        for (DeviceEntity device : deviceList) {
            this.TurnOff(device);
        }
    }

    public void TurnOff(DeviceEntity device) {
        DeviceOffTask deviceOffTask = new DeviceOffTask(device, domoticzService);
        queueThreadFactory.newThread(deviceOffTask).start();
    }

    public void TurnOn(List<DeviceEntity> deviceList) {
        for (DeviceEntity device : deviceList) {
            this.TurnOn(device);
        }
    }

    public void TurnOn(DeviceEntity device) {
        DeviceOnTask deviceOnTask = new DeviceOnTask(device, domoticzService);
        queueThreadFactory.newThread(deviceOnTask).start();
    }


}