package nl.jherder.modules.domotica.queue;

import nl.jherder.jdbi.DeviceEntity;
import nl.jherder.modules.domotica.services.DomoticzService;

public class DeviceOnTask implements Runnable {

    private DomoticzService domoticzService;
    private DeviceEntity device;

    DeviceOnTask(DeviceEntity device, DomoticzService domoticzService) {
        this.device = device;
        this.domoticzService = domoticzService;
    }

    @Override
    public void run() {
        domoticzService.switchOn(device.getReference());
    }

}