package nl.jherder.modules.domotica.queue;

import nl.jherder.jdbi.DeviceEntity;
import nl.jherder.modules.domotica.services.DomoticzService;

public class DeviceOffTask implements Runnable {

    private DomoticzService domoticzService;
    private DeviceEntity device;

    DeviceOffTask(DeviceEntity device, DomoticzService domoticzService) {
        this.device = device;
        this.domoticzService = domoticzService;
    }

    @Override
    public void run() {
        domoticzService.switchOff(device.getReference());
    }

}
