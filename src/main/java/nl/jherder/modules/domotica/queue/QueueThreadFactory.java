package nl.jherder.modules.domotica.queue;

import java.util.concurrent.ThreadFactory;

public class QueueThreadFactory implements ThreadFactory {

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r);
    }

}