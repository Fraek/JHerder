package nl.jherder.modules.domotica.defines;

public enum Status {

    UNKNOWN(0),
    ON(1),
    OFF(2);

    private final int value;
    private Status(int value){
        this.value = value;
    }
    public int value(){
        return value;
    }
}