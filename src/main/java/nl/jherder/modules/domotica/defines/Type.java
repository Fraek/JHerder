package nl.jherder.modules.domotica.defines;

public enum Type {

    UNKNOWN(0),
    SENSOR(1),
    ACTOR(2),
    IGNORED(3);

    private final int value;
    private Type(int value){
        this.value = value;
    }
    public int value(){
        return value;
    }
}