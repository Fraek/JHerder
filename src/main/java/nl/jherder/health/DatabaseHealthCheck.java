package nl.jherder.health;

import com.codahale.metrics.health.HealthCheck;
import org.hibernate.boot.model.relational.Database;

public class DatabaseHealthCheck extends HealthCheck {

    private final Database database;

    protected Result check() throws Exception {
        return Result.healthy();
    }

    public DatabaseHealthCheck(Database database) {
        this.database = database;
    }

}
