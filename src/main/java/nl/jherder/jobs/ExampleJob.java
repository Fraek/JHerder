package nl.jherder.jobs;

import com.google.inject.Inject;
import de.spinscale.dropwizard.jobs.Job;
import de.spinscale.dropwizard.jobs.annotations.Every;
import nl.jherder.modules.domotica.services.ZoneService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.context.internal.ManagedSessionContext;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.skife.jdbi.v2.sqlobject.Transaction;

import java.text.MessageFormat;

@Every(value = "60s", jobName = "ExampleJob")
public class ExampleJob extends Job {

    @Inject
    private ZoneService zoneService;

    @Inject
    private SessionFactory sessionFactory;

    @Override
    @Transaction
    public void doJob(JobExecutionContext context) throws JobExecutionException {
        //Open and bind a session
        Session session = sessionFactory.openSession();
        ManagedSessionContext.bind(session);

        //Hibernate has session from here
        int numberOfServices = zoneService.findAll().size();

        String msg = MessageFormat.format("Currently we have {0} services registered", numberOfServices);
        System.out.println(msg);

        //Close the session
        session.close();
    }

}
