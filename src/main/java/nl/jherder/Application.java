package nl.jherder;

import com.google.inject.Injector;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import nl.jherder.core.bundles.FlywayBundle;
import nl.jherder.core.bundles.GeoApiBundle;
import nl.jherder.core.bundles.GuiceJobsBundle;
import nl.jherder.core.bundles.HbnBundle;
import nl.jherder.core.filters.CharsetFilter;
import nl.jherder.core.modules.*;
import nl.jherder.health.DatabaseHealthCheck;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import ru.vyarus.dropwizard.guice.GuiceBundle;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class Application extends io.dropwizard.Application<AppConfiguration> {

    final private HbnBundle hibernate = new HbnBundle();
    final private FlywayBundle flyway = new FlywayBundle();

    private static GuiceBundle guiceBundle;

    public static void main(String[] args) throws Exception {
        new Application().run(args);
    }

    @Override
    public void initialize(final Bootstrap<AppConfiguration> bootstrap) {

        guiceBundle = GuiceBundle.builder()
                .enableAutoConfig("nl.jherder")
                .modules(
                        new HbnModule(hibernate),
                        new HttpModule(),
                        new AtmosphereModule(),
                        new JobsModule(),
                        new MapperModule(),
                        new GeoApiBundle()
                )
                .build();

        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(flyway);
        bootstrap.addBundle(guiceBundle);
        bootstrap.addBundle(new GuiceJobsBundle(guiceBundle));
    }

    @Override
    public void run(AppConfiguration configuration, Environment environment) {
        final DatabaseHealthCheck databaseCheck = new DatabaseHealthCheck(null);

        environment.jersey().register(CharsetFilter.class);

        //@ToDo clean this up
        FilterRegistration.Dynamic corsFilter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        environment.healthChecks().register("database", databaseCheck);
    }

    public static Injector getInjector() {
        return Application.guiceBundle.getInjector();
    }
}