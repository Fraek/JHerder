package nl.jherder.resources;

import nl.jherder.models.SystemMetric;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/metric")
@Produces(MediaType.APPLICATION_JSON)
public class MetricResource {

    @GET
    public SystemMetric index() {
        return new SystemMetric();
    }

}
