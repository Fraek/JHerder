package nl.jherder.jdbi;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "user_location", schema = "jherder", catalog = "")
@NamedQueries({
        @NamedQuery(name = "UserLocationEntity.findAll", query = "SELECT d FROM UserLocationEntity d"),
        @NamedQuery(name = "UserLocationEntity.findAllByUser", query = "SELECT d FROM UserLocationEntity d WHERE d.userByUserId = :user ORDER BY d.creationTime DESC")
})
public class UserLocationEntity implements IEntity{
    private long id;
    private Timestamp creationTime;
    private double lat;
    private double lon;
    private int battery;
    private Integer accuracy;
    private String connection;
    private UserEntity userByUserId;

    public static UserLocationEntity Default(){
        UserLocationEntity userLocationEntity = new UserLocationEntity();
        userLocationEntity.id = -1;
        return userLocationEntity;
    }

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "creation_time")
    public Timestamp getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    @Basic
    @Column(name = "lat")
    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @Basic
    @Column(name = "lon")
    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Basic
    @Column(name = "battery")
    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserLocationEntity that = (UserLocationEntity) o;

        if (id != that.id) return false;
        if (battery != that.battery) return false;
        if (creationTime != null ? !creationTime.equals(that.creationTime) : that.creationTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        long result = id;
        result = 31 * result + (creationTime != null ? creationTime.hashCode() : 0);
        result = 31 * result + battery;
        return (int)result;
    }

    @Basic
    @Column(name = "accuracy")
    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    @Basic
    @Column(name = "connection")
    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public UserEntity getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(UserEntity userByUserId) {
        this.userByUserId = userByUserId;
    }
}
