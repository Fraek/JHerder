package nl.jherder.jdbi;

import com.fasterxml.jackson.annotation.JsonView;
import nl.jherder.modules.domotica.models.views.DeviceView;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "device")
@NamedQueries({
        @NamedQuery(name = "Device.findAll", query = "SELECT d FROM Device d"),
        @NamedQuery(name = "Device.findAllSensor", query = "SELECT d FROM Device d WHERE d.type = 1"),
        @NamedQuery(name = "Device.findAllActor", query = "SELECT d FROM Device d WHERE d.type = 2"),
        @NamedQuery(name = "Device.findAllIgnored", query = "SELECT d FROM Device d WHERE d.type = 3"),
        @NamedQuery(name = "Device.findAllByName", query = "SELECT d FROM Device d WHERE d.name = :deviceName"),
        @NamedQuery(name = "Device.findAllByType", query = "SELECT d FROM Device d JOIN d.deviceType t WHERE d.type <> 3 AND t.name = :typeName "),
        @NamedQuery(name = "Device.findAllByTypeAndZone", query = "SELECT d FROM Device d JOIN d.deviceType t JOIN d.zone z WHERE d.type <> 3 AND t.name = :typeName AND z.name = :zoneName ")
})
@Deprecated
public class Device implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "reference")
    public Long reference;

    public Integer type;

    @ManyToOne()
    @JoinColumn(name = "device_type")
    @JsonView(DeviceView.Detail.class)
    private DeviceType deviceType;

    @Column(name = "device_type", updatable = false, insertable = false)
    @JsonView(DeviceView.List.class)
    private Long deviceTypeId;

    @ManyToOne()
    @JoinColumn(name = "zone")
    @JsonView(DeviceView.Detail.class)
    private Zone zone;

    @Column(name = "zone", updatable = false, insertable = false)
    @JsonView(DeviceView.List.class)
    private Long zoneId;

    @Column(name = "status")
    public Integer status = 0;

    @Column(name = "value")
    public String value;

    @Column(name = "name", nullable = false, unique = true)
    @NotNull
    public String name;

    @Column(name = "description")
    public String description;

    @JsonView(DeviceView.Detail.class)
    @Column(name = "creation_time")
    public DateTime createdTime;

    @JsonView(DeviceView.Detail.class)
    @Column(name = "update_time")
    public DateTime updatedTime;

    public Device() {
    }

    public Device(String name, String description, long reference) {
        this.name = name;
        this.description = description;
        this.reference = reference;
    }

    public void setUpdatedTime(DateTime updatedTime) {
        this.updatedTime = updatedTime;
    }

    public long getId() {
        return id;
    }

    public DeviceType getDeviceType() {
        if (deviceType == null) {
            this.setDeviceType(DeviceType.Default());
        }
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
        this.deviceTypeId = deviceType.getId();
    }

    public Long getDeviceTypeId() {
        return deviceTypeId;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
        this.zoneId = zone.getId();
    }

    public Long getZoneId() {
        return zoneId;
    }
}