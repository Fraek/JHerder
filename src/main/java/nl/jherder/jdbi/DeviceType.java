package nl.jherder.jdbi;

import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Table(name = "device_type")
@NamedQueries({
        @NamedQuery(name = "DeviceType.findAll", query = "SELECT dt FROM DeviceType dt")
})
@Deprecated
public class DeviceType implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "creation_time")
    @Transient
    private DateTime createdAt;

    @Column(name = "update_time")
    @Transient
    private DateTime updatedAt;

    public static DeviceType Default(){
        DeviceType defaultType = new DeviceType();
        defaultType.setId(-1);
        defaultType.setName("Unknown");
        defaultType.setDescription("Default DeviceType");
        return defaultType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public DeviceType update(DeviceType deviceType) {
        this.setName(deviceType.getName());
        this.setDescription(deviceType.getDescription());
        return this;
    }
}
