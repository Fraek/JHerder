package nl.jherder.jdbi;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "device", schema = "jherder", catalog = "")
@NamedQueries({
    @NamedQuery(name = "DeviceEntity.findAll", query = "SELECT d FROM DeviceEntity d"),
    @NamedQuery(name = "DeviceEntity.findAllByType", query = "SELECT d FROM DeviceEntity d JOIN d.deviceType t WHERE d.type <> 3 AND t.name = :typeName"),
    @NamedQuery(name = "DeviceEntity.findAllByName", query = "SELECT d FROM DeviceEntity d WHERE d.name LIKE :deviceName"),
    @NamedQuery(name = "DeviceEntity.findAllByTypeAndZone", query = "SELECT d FROM DeviceEntity d JOIN d.deviceType t JOIN d.zone z WHERE d.type <> 3 AND t.name = :typeName AND z.name = :zoneName"),
})
public class DeviceEntity implements IEntity  {
    private int id;
    private String reference;
    private String name;
    private String description;
    private int type;
    private Timestamp creationTime;
    private Timestamp updateTime;
    private int status;
    private String value;
    private DeviceTypeEntity deviceType;
    private ZoneEntity zone;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "reference")
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "creation_time")
    public Timestamp getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    @Basic
    @Column(name = "update_time")
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "status")
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Basic
    @Column(name = "value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceEntity that = (DeviceEntity) o;

        if (id != that.id) return false;
        if (type != that.type) return false;
        if (status != that.status) return false;
        if (reference != null ? !reference.equals(that.reference) : that.reference != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (creationTime != null ? !creationTime.equals(that.creationTime) : that.creationTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (reference != null ? reference.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + type;
        result = 31 * result + (creationTime != null ? creationTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + status;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "device_type", referencedColumnName = "id")
    public DeviceTypeEntity getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceTypeEntity deviceTypeByDeviceType) {
        this.deviceType = deviceTypeByDeviceType;
    }

    @ManyToOne
    @JoinColumn(name = "zone", referencedColumnName = "id")
    public ZoneEntity getZone() {
        return zone;
    }

    public void setZone(ZoneEntity zoneByZone) {
        this.zone = zoneByZone;
    }
}
