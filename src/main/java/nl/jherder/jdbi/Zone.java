package nl.jherder.jdbi;

import com.fasterxml.jackson.annotation.JsonView;
import nl.jherder.modules.domotica.models.views.ZoneView;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "zone")
@NamedQueries({
        @NamedQuery(name = "Zone.findAll", query = "SELECT z FROM Zone z")
})
@Deprecated
public class Zone implements IEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @Column(name = "name", nullable = false, unique = true)
    @NotNull
    public String name;

    @Column(name = "creation_time")
    public DateTime createdTime;

    @Column(name = "update_time")
    public DateTime updatedTime;

    @OneToMany()
    @JoinColumn(name = "zone")
    @JsonView(ZoneView.Detail.class)
    public List<Device> deviceList;

    public Zone() {

    }

    public Zone(String name){
        this.name = name;
    }

    public long getId() {
        return id;
    }
}
