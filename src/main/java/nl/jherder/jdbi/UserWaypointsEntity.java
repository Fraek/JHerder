package nl.jherder.jdbi;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "user_waypoints", schema = "jherder", catalog = "")
@NamedQueries({
        @NamedQuery(name = "UserWaypointsEntity.findAll", query = "SELECT d FROM UserWaypointsEntity d"),
        @NamedQuery(name = "UserWaypointsEntity.findByUserId", query = "SELECT d FROM UserWaypointsEntity d WHERE d.userByUserId = :user")
})
public class UserWaypointsEntity implements IEntity{
    private long id;
    private Timestamp creationTime;
    private Timestamp updateTime;
    private UserEntity userByUserId;
    private WaypointEntity homeByHomeId;
    private WaypointEntity workByWorkId;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "creation_time")
    public Timestamp getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Timestamp creationTime) {
        this.creationTime = creationTime;
    }

    @Basic
    @Column(name = "update_time")
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserWaypointsEntity that = (UserWaypointsEntity) o;

        if (id != that.id) return false;
        if (creationTime != null ? !creationTime.equals(that.creationTime) : that.creationTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int)id;
        result = 31 * result + (creationTime != null ? creationTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public UserEntity getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(UserEntity userByUserId) {
        this.userByUserId = userByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "home_id", referencedColumnName = "id")
    public WaypointEntity getHomeByHomeId() {
        return homeByHomeId;
    }

    public void setHomeByHomeId(WaypointEntity homeByHomeId) {
        this.homeByHomeId = homeByHomeId;
    }

    @ManyToOne
    @JoinColumn(name = "work_id", referencedColumnName = "id")
    public WaypointEntity getWorkByWorkId() {
        return workByWorkId;
    }

    public void setWorkByWorkId(WaypointEntity workByWorkId) {
        this.workByWorkId = workByWorkId;
    }
}
