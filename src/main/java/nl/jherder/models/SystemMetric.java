package nl.jherder.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.management.OperatingSystemMXBean;
import sun.management.ManagementFactoryHelper;

public class SystemMetric {

    @JsonProperty("Process CPU Load")
    public double processCPULoad;
    @JsonProperty("System CPU Load")
    public double systemCPULoad;
    @JsonProperty("Total Physical Memory")
    public long totalPhysicalMemory;
    @JsonProperty("Free Physical Memory")
    public long freePhysicalMemory;
    @JsonProperty("Percentage Physical Memory")
    public double percentagePhysicalMemory;
    @JsonProperty("Total Swap Space")
    public long totalSwapSpace;
    @JsonProperty("Free Swap Space")
    public long freeSwapSpace;
    @JsonProperty("Percentage Swap Space")
    public double percentageSwapSpace;
    @JsonProperty("System Load")
    public double systemLoad;


    public SystemMetric() {
        this.refresh();
    }

    public void refresh() {
        OperatingSystemMXBean operatingSystem = (OperatingSystemMXBean) ManagementFactoryHelper.getOperatingSystemMXBean();
        this.processCPULoad = operatingSystem.getProcessCpuLoad();
        this.systemCPULoad = operatingSystem.getSystemCpuLoad();
        this.totalPhysicalMemory = operatingSystem.getTotalPhysicalMemorySize();
        this.freePhysicalMemory = operatingSystem.getFreePhysicalMemorySize();
        this.percentagePhysicalMemory = getPercentage(freePhysicalMemory, totalPhysicalMemory);
        this.totalSwapSpace = operatingSystem.getTotalSwapSpaceSize();
        this.freeSwapSpace = operatingSystem.getFreeSwapSpaceSize();
        this.percentageSwapSpace = getPercentage(freeSwapSpace, totalSwapSpace);
        this.systemLoad = operatingSystem.getSystemLoadAverage();
    }

    private double getPercentage(long free, long max){
        return ( (max-free) * 100 ) / max;
    }

}
