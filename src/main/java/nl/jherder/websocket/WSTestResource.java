package nl.jherder.websocket;

import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.cpr.DefaultBroadcaster;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/ws-test")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class WSTestResource {

    @Inject
    BroadcasterFactory broadcasterFactory;

    /**
     * Example that sends a string value to all connected websocket clients
     * url example: http://host:port/ws-test?message={message}
     */
    @GET
    public String broadcastMessage(@QueryParam("message") String message){
        broadcasterFactory.lookup(DefaultBroadcaster.class, "jherder", true).broadcast(message);
        return "Broadcasted " + message;
    }

}