CREATE TABLE `zone` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `creation_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `zone_uq_name` (name)
)
  COLLATE='latin1_swedish_ci'
  ENGINE=InnoDB
;

ALTER TABLE `device`
  ADD `zone` INT(11),
  ADD CONSTRAINT `fk_zone` FOREIGN KEY (`zone`) REFERENCES `zone`(id)
;