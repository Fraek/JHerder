CREATE TABLE `user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `creation_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` DATETIME NULL DEFAULT NULL,
  `first_name` VARCHAR(100),
  `last_name` VARCHAR(100),
  `email` VARCHAR(100),
  PRIMARY KEY (`id`)
)
  COLLATE='latin1_swedish_ci'
  ENGINE=InnoDB
;

ALTER TABLE `location`
  ADD `user_id` INT(11),
  DROP COLUMN `tracker_id`,
  ADD CONSTRAINT `fk_user` FOREIGN KEY (user_id) REFERENCES user(id)
;