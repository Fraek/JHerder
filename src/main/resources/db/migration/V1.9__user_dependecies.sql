ALTER TABLE `location`
  DROP FOREIGN KEY `fk_user`,
  ADD CONSTRAINT `fk_user_location_user` FOREIGN KEY (user_id) REFERENCES user(id)
;

RENAME TABLE
    `location` TO `user_location`;

CREATE TABLE `user_waypoints` (
  `id`            INT(11)  NOT NULL AUTO_INCREMENT,
  `creation_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time`   DATETIME NULL     DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_id`       INT(11),
  `home_id`       INT(11),
  `work_id`       INT(11),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_user_waypoints_user` FOREIGN KEY (user_id) REFERENCES user (id),
  CONSTRAINT `fk_user_waypoints_waypoint-home` FOREIGN KEY (home_id) REFERENCES waypoint (id),
  CONSTRAINT `fk_user_waypoints_waypoint-work` FOREIGN KEY (work_id) REFERENCES waypoint (id)
)
  COLLATE = 'latin1_swedish_ci'
  ENGINE = InnoDB;

ALTER TABLE `device`
  DROP FOREIGN KEY `fk_device_type`,
  DROP FOREIGN KEY `fk_zone`,
  ADD CONSTRAINT `fk_device_device_type` FOREIGN KEY (device_type) REFERENCES device_type (id),
  ADD CONSTRAINT `fk_device_zone` FOREIGN KEY (`zone`) REFERENCES `zone` (id);