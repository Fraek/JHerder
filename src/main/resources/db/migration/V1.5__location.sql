CREATE TABLE `location` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `creation_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lat` DECIMAL(9,6) NOT NULL,
  `lon` DECIMAL(9,6) NOT NULL,
  `battery` INT(3) NOT NULL,
  `tracker_id` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`)
)
  COLLATE='latin1_swedish_ci'
  ENGINE=InnoDB
;