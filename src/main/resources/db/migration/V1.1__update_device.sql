CREATE TABLE `device_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `creation_time` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uq_name` (name)
)
  COLLATE='latin1_swedish_ci'
  ENGINE=InnoDB
;

ALTER TABLE `device`
  ADD `status` ENUM('ON','OFF') NOT NULL,
  ADD `value` VARCHAR(256),
  ADD `device_type` INT(11),
  ADD CONSTRAINT `fk_device_type` FOREIGN KEY (device_type) REFERENCES device_type(id)
;