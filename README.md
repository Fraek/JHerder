# JHerder
The backend of Herder developed with the Dropwizard framework.

<br>

## Database Migrations

#### Usage
When used via the application itself:
`java -jar jherder-{version}.jar db [command] configuration.yml`

When used via the flyway CLI
`flyway [options] [command]`

| Name     | Description                                                                                  |
|----------|----------------------------------------------------------------------------------------------|
| migrate  | Migrates the database                                                                        |
| clean    | Drops all objects in the configured schemas                                                  |
| info     | Prints the details and status information about all migrations                               |
| validate | Validates the applied migrations against the ones available on the classpath                 |
| baseline | Baselines an existing database, excluding all migrations up to and including baselineVersion |
| repair   | Repairs the metadata table                                                                   |

For more information see: https://flywaydb.org/documentation/